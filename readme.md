# BANDPROMOBOX.COM

## Server part: NodeJS - MongoDB - JWT

### DOCUMENTATION : 
- NODE JS : 
  - https://nodejs.org/en/docs/
- EXPRESS :     
  - http://expressjs.com/fr/4x/api.html 
- AWS-SDK :
  - https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/getting-started-nodejs.html  
- NODEMAILER : 
  - https://nodemailer.com/about/
- TESTING :     
  - MOCHA  
    - https://mochajs.org/  
  - CHAI  
    - https://www.chaijs.com/  
  - CHAI-HTTP  
    - https://www.chaijs.com/plugins/chai-http/  
- ILLUSTRATIONS FREE :     
  - https://undraw.co 
- GIT :
  - https://git-scm.com/doc


### Dependencies : 
- npm install (only if you download the projet)

- npm i express
- npm i mongoose
- npm i nodemon
- npm i jsonwebtoken
- npm i aws-sdk
- npm i bcrypt
- npm i cors
- npm i dotenv
- npm i nodemailer
- npm i morgan (log for all requests informations)

<!-- not sure  -->
- npm i fs
- npm i file-type

### DevDependencies :
- npm i mocha -D
- npm i chai -D
- npm i chai-http -D
- npm install eslint --save-dev


## GIT Method : 
BASIC :
- git init 
- git remote add origin git@gitlab.com:RSylv/bpb_node_server.git
- git add .
- git commit -m "Initial commit"
- git push -u origin main

AFTER :
- git checkout -b branch_name
- (work...)
- git add -A
- git commit -m "..."
- git checkout master
- git merge branch_name
- git push

GET INFOS :
- git status
- git log


## .gitignore BASE FILE FOR NODE : 
- https://github.com/github/gitignore/blob/master/Node.gitignore


## ESLINT : 
Init a config file :
- npx eslint --init

Then run (script added manually at the start of the project) : 
- npm run lint
- npm run lint:fix
  
    
# BASIC SERVER RESPONSE TYPE : 

- message: {  
  - type: 'success' || 'error',  
  -  body: 'The message'  
- },  
- Other var..  

#### EX.
```
return res.status(200).send({
  message: {
    type: 'success',
    body: 'The user is logged in'
  },
  user
})
```
or  
```
return res.status(200).send({
  message: {
    type: 'success',
    body: 'The band was updated successfully'
  },
  band
})
```
or  
```
return res.status(422).send({
  message: {
    type: 'error',
    body: 'The password is incorrect'
  }
})
```




#TODO: 
- Déplacer le fichier temp de demande de site a la racine du server (Dossier Public?) 
- Cote client gerer l'erreur text metre a jour le site alors qu'aucun site n'a etait crée. ✔ 
- Verifier l'enregistrement du status "OnLine" en BDD. ✔ 
- Desactiver et informer avant mise en ligne du site cote front, l'utilisateur, sur le fait qu'il doit bien verifier ses informations car une fois la construction du site lancée la fonctionnalités sera desactiver le temps que le site soit en ligne !
- Implementer Stripe pour le paiement
- Reflechir a un workflow sur la construction / mise en ligne du site / mise a jour du site, et activation ou non en fonction du paiement
- Deleted id user in Get my data response
- Debug delete photo and song routes and functionnalities

- HASTA LA VISTA !