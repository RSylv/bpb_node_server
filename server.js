if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config()
}
const express = require('express')
const config = require('./server/utils/config.js')
require('./server/utils/db.config')
const logger = require('morgan')
const cors = require('cors')
const corsOptions = {
  // origin: 'https://www.bandpromobox.com',
  // origin: 'http://localhost:4200',
  origin: '*',
  optionsSuccessStatus: 200
}
const app = express()

app.use(cors(corsOptions))
app.use(express.static('./public'))
app.use(express.json({ limit: '50mb' }))
app.use(express.urlencoded({ parameterLimit: 100000, extended: true }))
app.use(logger('dev'))

/**
 * Routes
 */
app.use('/public', require('./server/routes/public.route.js'))
app.use('/auth', require('./server/routes/auth.route'))
app.use('/auth/user', require('./server/routes/user.route.js'))
app.use('/auth/band', require('./server/routes/band.route.js'))

/**
 * Home base url response
 */
app.get('/', (req, res) => {
  res.status(200).send('Welcome to the BandPromoBox Server')
})

/**
 * 404 - Not found page
 */
app.use((req, res, next) => {
  return res.status(404).send('Rien par ici ...')
})

app.listen(config.port, () => {
  console.log(`Server is running at \n- http://localhost:${config.port}`)
})

module.exports = app
