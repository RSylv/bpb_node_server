const jwt = require('jsonwebtoken')
const User = require('../utils/db.models').user
const Band = require('../modules/bands/model/Band')

/**
 * Verify the token.
 * @returns req.userId
 */
const verifyToken = (req, res, next) => {
  const token = req.headers['x-access-token']

  if (!token) {
    return res.status(403).send({
      message: {
        type: 'unauthorized',
        body: 'No token provided'
      }
    })
  }

  jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
    if (err) {
      return res.status(401).send({
        message: {
          type: 'unauthorized',
          body: 'Invalid token'
        }
      })
    }

    req.userId = decoded.id
    next()
  })
}

/**
 * Verify if a user is connected or not. (for the route "contact-us")
 * @returns req.userId
 */
const verifyUser = (req, res, next) => {
  const token = req.headers['x-access-token']

  if (token) {
    jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
      if (!err) {
        req.userId = decoded.id
      }
    })
  }

  next()
}

/**
 * Get the user with the band infos
 * @returns req.user
 */
const getUserAndBand = (req, res, next) => {
  User.findById(req.userId).populate('band').exec((err, user) => {
    if (err) {
      return res.status(500).send({
        message: {
          type: 'error',
          body: 'Error getting user'
        }
      })
    }

    if (!user) {
      return res.status(400).send({
        message: {
          type: 'error',
          body: 'User not found'
        }
      })
    }

    req.hasBand = typeof user.band !== 'undefined'
    req.user = user
    next()
  })
}

/**
 * Get the band populate by bandPhotos
 */
function verifyStorageSize (collectionName) {
  return async (req, res, next) => {
    const band = await Band.findById(req.user.band._id).populate({
      path: collectionName
    }).exec()

    // console.log('Entry : ', collectionName)
    // console.log('PHOTO : ', band.bandPhotos.length)
    // console.log('SONGS : ', band.bandSongs.length)

    let total = req.body.size
    let limit = 5240000

    if (collectionName === 'bandPhotos') {
      band.bandPhotos.forEach(e => { total += e.size })
    } else {
      band.bandSongs.forEach(e => { total += e.size })
      limit = 26210000
    }

    if (total > limit) {
      return res.status(406).send({
        message: {
          type: 'error',
          body: 'Storage size exceeds limit'
        }
      })
    }

    next()
  }
}

module.exports = {
  verifyToken,
  verifyUser,
  verifyStorageSize,
  getUserAndBand
}
