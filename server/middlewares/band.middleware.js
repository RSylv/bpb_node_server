const hasFileRequest = (req, res, next) => {
  const hasFileRequest =
    typeof req.body.techFile !== 'undefined' &&
    Object.entries(req.body.techFile.url).length > 0

  req.hasFile = hasFileRequest
  next()
}

module.exports = {
  hasFileRequest
}
