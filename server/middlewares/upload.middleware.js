const { uploadFile } = require('../utils/fileUpload/uploadFile')

const upload = async (req, res, next) => {
  if (typeof req.body.url !== 'undefined' && req.body.url !== '') {
    const url = req.body.url.slice(5, 10) !== 'image'
      ? `${req.user.band.bandNameSlug}/audio`
      : `${req.user.band.bandNameSlug}/photo`

    // console.log('TYPE : ', req.body.url.slice(5, 10))
    // console.log('URL : ', url)

    const isFileSaved = await uploadFile({
      file: req.body.url,
      type: req.body.type,
      path: url
    })

    // console.log('isFileSaved : ', isFileSaved)

    if (isFileSaved.status !== 'ok') {
      return res.status(500).send({
        message: {
          type: 'error',
          body: isFileSaved.message
        }
      })
    }

    req.key = isFileSaved.key
  }
  next()
}

module.exports = {
  upload
}
