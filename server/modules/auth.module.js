const User = require('./users/model/User')
const { addUser } = require('./users/service/user.service')
const { mongooseErrorFormatter } = require('../utils/dataFormatter/validation.formatter')
const { userSessionDataFormatter } = require('../utils/dataFormatter/userData.formatter')

/**
 * Register a user
 */
const register = async (req, res, next) => {
  try {
    await addUser(req.body)
    return res.status(200).send({
      message: {
        type: 'success',
        body: 'User was registered successfully'
      }
    })
  } catch (e) {
    return res.status(422).send({
      message: {
        type: 'error',
        body: 'Error during registration'
      },
      errors: mongooseErrorFormatter(e)
    })
  }
}

/**
 * Login a user
 */
const login = async (req, res, next) => {
  if (typeof req.body.email === 'undefined' || req.body.email === '') {
    return res.status(400).send({
      message: {
        type: 'error',
        body: 'Missing credentials'
      }
    })
  }
  try {
    const user = await User.findOne({ email: req.body.email })
    if (!user) {
      return res.status(404).send({
        message: {
          type: 'error',
          body: 'Email does not exist'
        }
      })
    }
    if (await user.checkPassword(req.body.password, user.password)) {
      await User.findOneAndUpdate({ _id: user._id }, { isOnline: true })
      const userSessionData = await userSessionDataFormatter(user)
      return res.status(200).send({
        message: {
          type: 'success',
          body: 'User is logged in'
        },
        data: userSessionData
      })
    }
    return res.status(401).send({
      message: {
        type: 'error',
        body: 'Password is incorrect'
      }
    })
  } catch (e) {
    console.log('LOGIN CATCH ERROR : ', e.message)
    return res.status(500).send({
      message: {
        type: 'error',
        body: 'Error during login'
      }
    })
  }
}

/**
 * Logout a user
 */
const logout = async (req, res, next) => {
  try {
    const user = await User.findById(req.userId)
    if (!user) {
      return res.status(404).send({
        message: {
          type: 'error',
          body: 'no user found'
        }
      })
    }
    await User.findOneAndUpdate({ _id: user._id }, { isOnline: false })
    return res.status(200).send({
      message: {
        type: 'success',
        body: 'User has been logged out'
      }
    })
  } catch (e) {
    console.log('LOGOUT ERROR: ', e)
    return res.status(500).send({
      message: {
        type: 'error',
        body: 'logout error'
      }
    })
  }
}

module.exports = {
  register,
  login,
  logout
}
