const { uploadFile, deleteFile, getSignedFileUrl } = require('../../utils/fileUpload/uploadFile')
const { getBand, addBand, updateBand, formatReqBodyInputs } = require('./service/band.service')
const { mongooseErrorFormatter } = require('../../utils/dataFormatter/validation.formatter')
const User = require('../users/model/User')

/**
 * Here, we save the band informations.
 * (name, member, events, technical file, contact, biographie ...)
 * Get the request status if a file is incomming and if band already exists (create ou update)
 * Then make the traitement :
 *  - save the file if present :
 *    - delete the old one if present
 *    - add it to the band object
 *  - save the band
 *  - update the user
 *  - get the new signed Url of the file and add it to the band object that we send to the client
 */
const saveBand = async (req, res) => {
  const user = req.user
  let band = req.hasBand

  try {
    const bandForm = await formatReqBodyInputs(req.body, user)

    /**
     * If request have file:
     * Save the file and delete the old one, if exist.
     */
    let isFileSaved = null
    if (req.hasFile) {
      const type = req.body.techFile.type
      const file = req.body.techFile.url

      /**
       * We save the file to the AWS bucket via our middleware service and functions.
       */
      isFileSaved = await uploadFile({
        file: file,
        type: type,
        path: `${bandForm.bandNameSlug}/techFile`
      })

      /**
      * If file is not saved, then return the error.
      */
      if (isFileSaved.status !== 'ok') {
        return res.status(500).send({ status: 'error', message: isFileSaved.message })
      }

      /**
       * Save the new values into Band object.
       */
      bandForm.techFileKey = isFileSaved.key
      bandForm.techFileType = type

      /**
       * Delete the old file, if exist.
       */
      if (typeof isFileSaved.key !== 'undefined' && isFileSaved.key !== '') {
        if (band && user.band.techFileKey !== '') {
          await deleteFile(user.band.techFileKey)
        }
      }
    }

    /**
     * Get the Signed URl for the techFile.
     */
    let fileUrlAuth = ''
    if (isFileSaved !== null && isFileSaved.key !== '') {
      fileUrlAuth = await getSignedFileUrl(isFileSaved.key, 3600)
    }

    /**
    * Infos Log.
    */
    // console.log(
    //   '\n>------------------------',
    //   '\nBand Request           : ',
    //   '\n-------------------------',
    //   '\n\nIsUpdate       :', band,
    //   '\nHasFileRequest :', req.hasFile,
    //   '\nIsFileSaved    :', isFileSaved,
    //   '\n\n---------------------------->\n'
    // )

    /**
     * If band exist: update the band.
     */
    if (band) {
      await updateBand(user.band._id, bandForm)
      const newBand = await getBand(user.band)
      newBand.techFileKey = fileUrlAuth
      return res.status(200).send({
        message: {
          type: 'success',
          body: 'Band was updated successfully'
        },
        band: newBand
      })
    }

    /**
     * Else: create the band and save band, and user.
     */
    band = await addBand(user, bandForm)
    band.techFileKey = fileUrlAuth

    return res.status(201).send({
      message: {
        type: 'success',
        body: 'Band created successfully'
      },
      band
    })
  } catch (e) {
    console.log(e.message)
    return res.status(422).send({
      message: {
        type: 'error',
        body: mongooseErrorFormatter(e)
      }
    })
  }
}

/**
 * Get the signed url of the techFile
 */
const getTechFileUrl = async (req, res) => {
  try {
    const user = await User.findById(req.userId).populate('band').exec()

    const techFileUrl = await getSignedFileUrl(user.band.techFileKey, 120)

    return res.status(200).send({
      message: {
        type: 'success',
        body: 'TechFile url getting with success'
      },
      techFileUrl
    })
  } catch (e) {
    console.log('ERROR : ', e)
    return res.status(400).send({
      message: {
        type: 'error',
        body: 'Error occurs, when getting signed file URL'
      }
    })
  }
}

module.exports = {
  saveBand,
  getTechFileUrl
}
