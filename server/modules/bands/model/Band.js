const mongoose = require('mongoose')

const BandSchema = mongoose.Schema({
  emailContact: {
    type: String,
    maxlength: [128, 'Email can\'t be more than 128 characters'],
    unique: [true, 'Email already exists'],
    match: [/.+@.+\..+/, 'Email is not valid']
  },
  videoLink: {
    type: String,
    maxlength: [255, 'Video link can\'t be more than 255 characters'],
    match: [/[+]?[(]?[0-9]{3}[)]?[-\s.]?[0-9]{3}[-\s.]?[0-9]{4,6}/, 'Url is not valid']
  },
  phoneContact: {
    type: String,
    maxlength: [12, 'phone cant be more than 12 characters'],
    default: ''
  },
  bandName: {
    type: String,
    maxlength: [120, 'Band name cant be more than 120 characters'],
    required: [true, 'Band name is required']
  },
  bandNameSlug: {
    type: String,
    maxlength: [120, 'Band name Slug cant be more than 120 characters']
  },
  biographie: {
    type: String,
    // minlength: [60, 'Biographie must be at least 60 characters'],
    maxlength: [5000, 'Biographie cant be more than 5000 characters']
  },
  members: [{
    pseudo: {
      type: String,
      minlength: [2, 'Pseudo must be at least 2 characters'],
      maxlength: [64, 'Pseudo cant be more than 64 characters']
    },
    role: {
      type: String,
      maxlength: [128, 'Role cant be more than 128 characters']
    }
  }],
  events: [{
    date: {
      type: Date
    },
    city: {
      type: String
    },
    zipCode: {
      type: String,
      maxlength: [5, 'Zip-Code cant be more than 5 characters']
    },
    place: {
      type: String,
      maxlength: [64, 'Place cant be more than 64 characters']
    },
    price: {
      type: Number,
      default: 0
    },
    hour: {
      type: String,
      maxlength: [5, 'Hour cant be more than 5 characters']
    }
  }],
  pageStyle: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'PageStyle',
    autopopulate: true
  },
  bandSongs: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Song',
    autopopulate: true
  }],
  bandPhotos: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Photo',
    autopopulate: true
  }],
  techFileKey: {
    type: String,
    maxlength: [255, 'TechFileKey cant be more than 255 characters'],
    default: ''
  },
  techFileType: {
    type: String,
    maxlength: [25, 'TechFileType cant be more than 25 characters'],
    default: ''
  }
},
{
  timestamps: true
})

// BandSchema.path('videoLink').validate((val) => {
//   const urlRegex = /[+]?[(]?[0-9]{3}[)]?[-\s.]?[0-9]{3}[-\s.]?[0-9]{4,6}/
//   return urlRegex.test(val)
// }, 'Invalid URL.')

const Band = mongoose.model('Band', BandSchema)

module.exports = Band
