const Band = require('../model/Band')
const { updateUser } = require('../../users/service/user.service')

/**
 * Create a new band and return it
 * @param {Object} bandInput - It is the band inputs values.
 */
const addBand = async (user, bandInput) => {
  const band = new Band(bandInput)
  await band.save()
  await updateUser(user.id, { band: band })
  return band
}

/**
 * Update a band and return it
 * @param {string} id - It is the id of the band
 * @param {object} bandInput - It is the band inputs to update. {key: value}
 */
const updateBand = async (bandId, bandInput) => {
  const band = await Band.findByIdAndUpdate({ _id: bandId }, bandInput)
  return band
}

/**
 * Return the band with full informations *( pageStyle, bandSongs, bandPhotos )*
 * @param {string} bandId - The id of the user band.
 */
const getBand = async (bandId) => {
  const band = await Band.findById(bandId)
    .populate('pageStyle')
    .populate('bandSongs')
    .populate('bandPhotos')
    .exec()
  return band
}

/**
 * Create band object with form values of the request.
 * @param {object} reqBody
 */
const formatReqBodyInputs = async (reqBody, user) => {
  const bandForm = {
    bandName: reqBody.bandName,
    videoLink: reqBody.videoLink,
    bandNameSlug: reqBody.bandNameSlug,
    emailContact: reqBody.emailContact,
    phoneContact: reqBody.phoneContact,
    biographie: reqBody.biographie,
    members: reqBody.members,
    events: reqBody.events,
    techFileKey: checkTypeAndKey(user.band, 'key'),
    techFileType: checkTypeAndKey(user.band, 'type')
  }
  return bandForm
}

/**
 * Check if the values (bgImageType & bgImageKey) are set, and return it or empty string.
 */
function checkTypeAndKey (userBand, selector) {
  let value = ''
  if (typeof userBand !== 'undefined' && userBand !== null) {
    if (selector === 'type') {
      value = userBand.techFileType
    }
    if (selector === 'key') {
      value = userBand.techFileKey
    }
  }
  return value
}

module.exports = {
  addBand,
  updateBand,
  getBand,
  formatReqBodyInputs
}
