const mongoose = require('mongoose')

const PhotoModel = mongoose.Schema({
  type: {
    type: String
  },
  path: {
    type: String
  },
  size: {
    type: Number
  },
  readableSize: {
    type: String
  },
  name: {
    type: String,
    maxlength: [50, 'Max Length is 50 characters'],
    unique: true
  },
  description: {
    type: String,
    maxlength: [50, 'Max Length is 50 characters']
  },
  key: {
    type: String,
    required: true,
    maxlength: [255, 'Max Length is 255 characters']
  }
},
{
  timestamps: true
})

const Photo = mongoose.model('Photo', PhotoModel)

module.exports = Photo
