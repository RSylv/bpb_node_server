const { deleteFile, getSignedFileUrl } = require('../../utils/fileUpload/uploadFile')
const { mongooseErrorFormatter } = require('../../utils/dataFormatter/validation.formatter')
const Photo = require('./model/BandPhoto')
const User = require('../users/model/User')
const Band = require('../bands/model/Band')
const { updateBand } = require('../bands/service/band.service')

// const LIMIT_STORAGE_SIZE = 5240000

/**
 * Save a photo in the DB, and update the user band.
 */
const savePhoto = async (req, res) => {
  try {
    const band = await Band.findById(req.user.band._id).populate('bandPhotos').exec()

    // let total = req.body.size
    // band.bandPhotos.forEach(e => { total += e.size })
    // if (total > LIMIT_STORAGE_SIZE) {
    //   return res.status(406).send({
    //     message: {
    //       type: 'error',
    //       body: 'Storage size exceeds limit'
    //     }
    //   })
    // }

    const photo = await new Photo({
      type: req.body.type,
      path: `${band.bandNameSlug}/photo`,
      size: req.body.size,
      readableSize: req.body.readableSize,
      name: req.body.newName,
      description: req.body.description.desc,
      key: req.key
    })

    band.bandPhotos.push(photo)
    await photo.save()
    await updateBand(band._id, { bandPhotos: band.bandPhotos })

    photo.path = await getSignedFileUrl(photo.key, 3600)

    return res.send({
      message: {
        type: 'success',
        body: `The Photo ${req.body.newName} was saved and added to band successfully!`
      },
      photo: photo
    })
  } catch (e) {
    return res.status(500).send({
      message: {
        type: 'error',
        body: mongooseErrorFormatter(e)
      }
    })
  }
}

/**
 * Delete an image of the DB and AWS s3 Bucket.
 */
const deletePhoto = async (req, res) => {
  let photo = null
  try {
    // Get Photo
    photo = await Photo.findById(req.params.id).exec()
    // Get user.band
    const band = await Band.findById(req.user.band._id).populate('bandPhotos').exec()

    // Get index
    const idx = band.bandPhotos.indexOf(req.params.id)

    // Delete element in array
    band.bandPhotos.splice(idx, 1)

    // Save the new bandPhotos array/collection to the DB
    await updateBand(band._id, { bandPhotos: band.bandPhotos })

    // Delete the file from AWS bucket
    await deleteFile(photo.key)

    // Remove the photo from the DB
    await photo.remove()

    // Return statement
    res.send({
      message: {
        type: 'success',
        body: 'The Photo was deleted successfully!'
      },
      photoDeleteId: photo._id
    })
    return
  } catch (e) {
    console.log('DELETE PHOTO CATCH ERROR: ', e)

    if (photo == null) {
      res.status(404).send({
        message: {
          type: 'error',
          body: 'No Photo found'
        }
      })
    } else {
      res.status(500).send({
        message: {
          type: 'error',
          body: 'Error when delete photo. Please retry or contact us'
        }
      })
    }
  }
}

/**
 * Get the user photos with AWS signed url.
 * @returns user photos
 */
const getUserPhotosUrl = async (req, res) => {
  try {
    const user = await User.findById(req.userId).populate({
      path: 'band',
      populate: {
        path: 'bandPhotos'
      }
    }).exec()

    if (!user) { return res.status(404).send({ status: 'error', message: 'no user found.' }) }

    for (let i = 0; i < user.band.bandPhotos.length; i++) {
      user.band.bandPhotos[i].path = await getSignedFileUrl(user.band.bandPhotos[i].key, 3600)
    }

    return res.status(200).send({ status: 'ok', photos: user.band.bandPhotos })
  } catch (e) {
    console.log('GetUserPhotosUrl: ', e)

    return res.status(500).send({ status: 'error', message: 'Error during process: ' + e.message })
  }
}

module.exports = {
  savePhoto,
  deletePhoto,
  getUserPhotosUrl
}
