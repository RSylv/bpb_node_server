const { getSignedFileUrl } = require('../utils/fileUpload/uploadFile')
const Band = require('./bands/model/Band')
const nodemailer = require('nodemailer')
const { htmlspecialchars } = require('../utils/tools.js')

/**
 * Get the 20 last saved Song, for the public homepage audio Player.
 */
const getPublicSongs = async (req, res) => {
  try {
    // console.log('\nSONG REQUEST\n')

    /**
     * Get the 20 last register band.
     */
    const bands = await Band.find({})
      .populate({ path: 'bandSongs', select: ['path', 'name', 'description', 'modifiedAt', 'key'] })
      .populate('pageStyle')
      .sort({ modifiedAt: -1 }).limit(20)
      .exec()

    const songs = []

    /**
     * We construct an audio object with only one song, (if song exist) :
     * - the bandName (artist)
     * - image (cover)
     * - song (the first song)
     *  - path
     *  - name
     *  - modifiedAt
     */
    for (let i = 0; i < bands.length; i++) {
      if (bands[i].bandSongs.length > 0) {
        bands[i].bandSongs[0].path = await getSignedFileUrl(bands[i].bandSongs[0].key, 3600)
        let photo = ''

        if (bands[i].pageStyle !== undefined &&
                    bands[i].pageStyle.bgImageKey !== '') {
          photo = await getSignedFileUrl(bands[i].pageStyle.bgImageKey, 3600)
        }

        songs.push({
          artist: bands[i].bandName,
          photo,
          song: {
            path: bands[i].bandSongs[0].path,
            name: bands[i].bandSongs[0].name,
            modifiedAt: bands[i].bandSongs[0].modifiedAt
          }
        })
      }
    }

    if (songs === []) {
      return res.status(204).send('No songs yet.')
    }

    // console.log(songs)
    return res.status(200).send({ data: songs })
  } catch (e) {
    console.log('Error getting public songs.', e)
    return res.status(500).send({ status: 'error', message: 'Error getting public songs.' })
  }
}

/**
 * Get the message data (name, email, phone and message), and send the mail to bandpromobox@gmail.com
 */
const contactUs = (req, res) => {
  const user = {
    name: htmlspecialchars(req.body.name),
    email: htmlspecialchars(req.body.email),
    phone: req.body.phone,
    message: htmlspecialchars(req.body.message)
  }
  // if user exist in db
  const userID = req.userId || null

  // Create transporter (mail options)
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'bandpromobox@gmail.com',
      pass: 'b6h7Zr6izq9JrcP'
    }
  })

  // Create mail-object
  const mailContent = {
    from: 'BandPromoBox.com',
    to: 'bandpromobox@gmail.com',
    subject: 'BPB: Contact Us - message',
    html: `<div>BandPromoBox.com</div>
    <div style="display:flex;flex-direction:column;justify-content:center; align-items:center;padding: 1.5rem;">
      <h1 style="margin:0">New Message</h1>
      <h2 style="margin:.5rem">Contact Us</h2>
      <table style="padding: .5rem 1rem;border: 1px solid #000;border-radius:.25rem;margin:1rem 0;">
        <tr style="padding:1.5rem;">
          <td style="color:#414141;padding-right:1rem;">ID</td>
          <td>${userID !== null ? userID : 'not register'}</td>
        </tr>
        <tr style="padding:1.5rem;">
          <td style="color:#414141;padding-right:1rem;">name</td>
          <td>${user.name || '--'}</td>
        </tr>
        <tr style="padding:1.5rem 0;">
          <td style="color:#414141;padding-right:1rem;">email</td>
          <td>${user.email}</td>
        </tr>
        <tr style="padding:1.5rem 0;">
          <td style="color: #414141;padding-right:1rem;">phone</td>
          <td>${user.phone || '--'}</td>
        </tr>
      </table>
      <table style="padding: .5rem 1.5rem;margin-top: 1rem;">
        <tr style="display:flex;flex-direction:column">
          <td style="background:rgb(65, 65, 65);color:rgb(255, 255, 255);padding:.25rem .75rem; width:max-content;border-radius:.25rem;margin-bottom:.5rem">message</td>
          <td>${user.message}</td>
        </tr>
      </table>
    </div>`
  }

  // Send mail function, and return answer
  transporter.sendMail(mailContent, async (error, info) => {
    if (error) {
      return res.status(500).send({
        message: {
          type: 'error',
          body: 'Message not sent.'
        }
      })
    } else {
      return res.status(200).send({
        message: {
          type: 'success',
          body: 'Thank you, you\'re message is sent !'
        }
      })
    }
  })
}

module.exports = {
  getPublicSongs,
  contactUs
}
