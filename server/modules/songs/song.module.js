const { deleteFile, getSignedFileUrl } = require('../../utils/fileUpload/uploadFile')
const { updateBand } = require('../bands/service/band.service')
const Song = require('./model/BandSong')
const Band = require('../bands/model/Band')

// const LIMIT_STORAGE_SIZE = 26210000

/**
 * Save the song in the DB and update the user band.
 */
const saveSong = async (req, res) => {
  try {
    const band = await Band.findById(req.user.band._id).populate('bandSongs').exec()

    // let total = req.body.size
    // band.bandSongs.forEach(e => { total += e.size })
    // if (total > LIMIT_STORAGE_SIZE) {
    //   return res.status(406).send({
    //     message: {
    //       type: 'error',
    //       body: 'Storage size exceeds limit'
    //     }
    //   })
    // }

    const song = await new Song({
      type: req.body.type,
      path: `${band.bandNameSlug}/audio`,
      size: req.body.size,
      readableSize: req.body.readableSize,
      name: req.body.newName,
      description: 'no description..',
      key: req.key
    })

    band.bandSongs.push(song)
    await song.save()
    await updateBand(band._id, { bandSongs: band.bandSongs })

    song.path = await getSignedFileUrl(song.key, 3600)

    return res.status(201).send({
      message: {
        type: 'success',
        body: `The song ${req.body.newName} was saved and added to band successfully!`
      },
      song: song
    })
  } catch (e) {
    console.log('SAVESONG ERROR: ', e)
    return res.status(500).send({
      message: {
        type: 'error',
        body: 'failed to upload song'
      }
    })
  }
}

/**
 * Delete a song of the DB and AWS s3 Bucket.
 */
const deleteSong = async (req, res) => {
  let song = null
  try {
    // Get Song
    song = await Song.findById(req.params.id).exec()
    // Get user.band
    const band = await Band.findById(req.user.band._id).exec()
    // Get index
    const idx = band.bandSongs.indexOf(req.params.id)
    // Delete element in array
    band.bandSongs.splice(idx, 1)
    // Save the new bandSongs collection in DB
    await updateBand(band._id, { bandSongs: band.bandSongs })
    // Delete the file from AWS Storage
    await deleteFile(song.key)
    // Remove the song from DB
    await song.remove()

    // Return statement
    return res.send({
      message: {
        type: 'success',
        body: 'The Song was deleted successfully!'
      },
      deleteSongId: song._id
    })
  } catch (e) {
    console.log('DELETE PHOTO CATCH ERROR: ', e)

    if (song == null) {
      return res.status(404).send({
        message: {
          type: 'error',
          body: 'No Song found'
        }
      })
    } else {
      return res.status(500).send({
        message: {
          type: 'error',
          body: 'Error when delete song. Please retry or contact us'
        }
      })
    }
  }
}

module.exports = {
  saveSong,
  deleteSong
}
