const mongoose = require('mongoose')

const PageStyleModel = mongoose.Schema({
  bgImageType: {
    type: String
  },
  bgImageKey: {
    type: String,
    unique: true
  },
  bgColor: {
    type: String,
    maxlength: [10, 'Max Length is 10 characters']
  },
  titleColor: {
    type: String,
    maxlength: [10, 'Max Length is 10 characters']
  },
  textColor: {
    type: String,
    maxlength: [10, 'Max Length is 10 characters']
  },
  bandNameColor: {
    type: String,
    maxlength: [10, 'Max Length is 10 characters']
  },
  shadowColor: {
    type: String,
    maxlength: [10, 'Max Length is 10 characters']
  },
  font: {
    type: String
  }
},
{
  timestamps: true
})

const PageStyle = mongoose.model('PageStyle', PageStyleModel)

module.exports = PageStyle
