const { uploadFile, deleteFile, getSignedFileUrl } = require('../../utils/fileUpload/uploadFile')
const Style = require('./model/PageStyle')
// const User = require('../users/model/User')
const Band = require('../bands/model/Band')
const { updateBand } = require('../bands/service/band.service')

/**
 * Here we save the user style choices for the band Site.
 *
 * We've got to check if there's a image, save it and then save the style
 * and the user bandStyle.
 *
 */
const savePageStyle = async (req, res) => {
  /**
   * Get the band, and check if it exist.
   * (PUT THIS CODE IN A MIDDLEWARE FUNCTION)
   */
  const band = await Band.findById(req.user.band._id).populate({
    path: 'pageStyle'
  }).exec()
  if (typeof band === 'undefined') {
    return res.status(204).send({
      message: {
        type: 'error',
        body: 'No band register yet.'
      }
    })
  }

  const update = typeof band.pageStyle !== 'undefined'
  const hasFileRequest = req.body.bgImage !== ''

  /**
   * Create Style object with form values
   */
  const pageFormStyle = {
    bgImageType: checkTypeAndKey(band.pageStyle, 'type'),
    bgImageKey: checkTypeAndKey(band.pageStyle, 'key'),
    bgColor: req.body.bgColor,
    titleColor: req.body.titleColor,
    textColor: req.body.textColor,
    bandNameColor: req.body.bandNameColor,
    shadowColor: req.body.shadowColor,
    font: req.body.font
  }

  try {
    /**
     * If request have file:
     * Save the File And Delete the old one, if exist
     */
    let isFileSaved = null
    if (hasFileRequest) {
      /**
       * We save the file to the AWS bucket via our middleware service and functions.
       */
      isFileSaved = await uploadFile({
        file: req.body.bgImage,
        type: req.body.bgImageType,
        path: `${band.bandNameSlug}/imgBack`
      })

      /**
       * If file is not saved, then return the error.
       */
      if (isFileSaved.status !== 'ok') {
        return res.status(500).send({
          message: {
            type: 'error',
            body: isFileSaved.message
          }
        })
      }

      /**
       * Save the new file values into Style object
       */
      pageFormStyle.bgImageKey = isFileSaved.key
      pageFormStyle.bgImageType = req.body.bgImageType

      /**
       * Delete the old image, if exist.
       */
      if (typeof isFileSaved.key !== 'undefined' && isFileSaved.key !== '') {
        if (update && band.pageStyle !== null && band.pageStyle.bgImageKey !== '') {
          await deleteFile(band.pageStyle.bgImageKey)
        }
      }
    }

    /**
     * Get the Signed URl for the image.
     */
    let fileUrlAuth = ''
    if (isFileSaved !== null && isFileSaved.key !== '') {
      fileUrlAuth = await getSignedFileUrl(isFileSaved.key, 3600)
    }

    /**
     * if Update: update the style
     */
    if (update) {
      const savedStyle = await Style.findByIdAndUpdate(band.pageStyle, pageFormStyle, { returnOriginal: false })
      return res.send({
        message: {
          type: 'success',
          body: 'The Style was updated successfully!'
        },
        fileUrl: fileUrlAuth,
        style: savedStyle
      })
    }

    /**
     * if Not Update: Create the style.
     */
    const style = new Style(pageFormStyle)
    await style.save()
    band.pageStyle = style
    await updateBand(band._id, { pageStyle: style })

    /**
     * Return the succed message,
     * the fileUrl of the image, and
     * the style saved.
     */
    return res.status(201).send({
      message: {
        type: 'success',
        body: 'The Style was saved and added to band successfully!'
      },
      fileUrl: fileUrlAuth,
      style
    })
  } catch (e) {
    console.log('savePageStyle error: ', e)

    return res.status(500).send({
      message: {
        type: 'error',
        body: 'Error saving page style'
      }
    })
  }
}

/**
 * Check if the values (bgImageType & bgImageKey) already exist in user DB pageStyle object.
 */
function checkTypeAndKey (userStyle, selector) {
  let value = ''

  if (typeof userStyle !== 'undefined' && userStyle !== null) {
    if (selector === 'type') {
      value = userStyle.bgImageType
    }
    if (selector === 'key') {
      value = userStyle.bgImageKey
    }
  }
  return value
}

module.exports = {
  savePageStyle
}
