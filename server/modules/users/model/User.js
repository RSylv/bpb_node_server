const mongoose = require('mongoose')
const bcrypt = require('bcrypt')

const UserSchema = mongoose.Schema({
  username: {
    type: String,
    required: [true, 'Name is required'],
    minlength: [2, 'Name must be at least 2 characters'],
    maxlength: [64, 'Name cant be more than 64 characters']
  },
  email: {
    type: String,
    required: [true, 'Email is required'],
    maxlength: [128, 'Email cant be more than 128 characters'],
    unique: [true, 'Email already exists'],
    match: [/.+@.+\..+/, 'Email is not valid']
  },
  password: {
    type: String,
    required: [true, 'Password is required'],
    minlength: [8, 'Password must be at least 8 characters'],
    maxlength: [30, 'Password cant be more than 128 characters']
  },
  band: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Band',
    autopopulate: true
  },
  hasSubscribe: {
    type: Boolean,
    required: true,
    default: false
  },
  isOnMailingList: {
    type: Boolean,
    required: true,
    default: false
  },
  isBanned: {
    type: Boolean,
    required: true,
    default: false
  },
  hasSiteActive: {
    type: Boolean,
    required: true,
    default: false
  },
  bandSiteUrl: {
    type: String,
    required: true,
    default: 'none'
  },
  isOnline: {
    type: Boolean,
    required: true,
    default: false
  },
  hasFileSaved: {
    type: Boolean,
    required: true,
    default: false
  },
  roles: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Role'
    }
  ]
},
{
  timestamps: true
})

/**
 * Validates unique email
 */
UserSchema.path('email').validate(async (email) => {
  // console.log('EMAIL SAVED : ', this.email)
  // console.log('EMAIL RECIEVED : ', email)
  const emailCount = await mongoose.models.User.countDocuments({ email })
  return !emailCount
}, 'Email already exists')

/**
 * Encrypt password if value is changed
 */
UserSchema.pre('save', function (next) {
  if (!this.isModified('password')) {
    return next()
  }

  bcrypt.hash(this.password, 10, (err, hash) => {
    if (err) {
      return next(err)
    }
    this.password = hash
    next()
  })
})

/**
 * Check paswword
 * @param {string} password - The password from input
 * @param {string} hashedPaswword - The user saved password
 */
UserSchema.methods.checkPassword = async (password, hashedPaswword) => {
  const res = await bcrypt.compare(password, hashedPaswword)
  return res
}

const User = mongoose.model('User', UserSchema)

module.exports = User
