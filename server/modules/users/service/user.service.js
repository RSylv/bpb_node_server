const User = require('../model/User')

/**
 * Create a new user and return it
 * @param {Object} userInput - It is the user inputs with all the variables set.
 */
const addUser = async (userInput) => {
  const user = new User(userInput)
  await user.save()
  return user
}

/**
 * Update a user and return it
 * @param {string} id - It is the id of the user
 * @param {Object} userInput - It is the user inputs to update.
 */
const updateUser = async (id, userInput) => {
  const user = await User.findByIdAndUpdate({ _id: id }, userInput)
  return user
}

const deleteUser = async (id) => {
  try {
    const user = await User.findById(id).exec()
    await user.delete()
    return { message: 'User deleted' }
  } catch (e) {
    return e
  }
}

module.exports = {
  addUser,
  updateUser,
  deleteUser
}
