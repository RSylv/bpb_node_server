const { deleteFile, downloadPersonalData } = require('../../utils/fileUpload/uploadFile')
const { createMyBandSite } = require('../../utils/siteCreator/siteCreator')
const { updateUser } = require('./service/user.service')
const { mongooseErrorFormatter } = require('../../utils/dataFormatter/validation.formatter')
const User = require('./model/User')
const Band = require('../bands/model/Band')
const Style = require('../styles/model/PageStyle')
const Song = require('../songs/model/BandSong')
const Photo = require('../photos/model/BandPhoto')
const bcrypt = require('bcrypt')
const nodemailer = require('nodemailer')

/**
 * Delete ALL the data of the account.
 * on AWS and DATABASE.
 */
const deleteAccount = async (req, res) => {
  // Check authencity of the User
  if (req.params.id !== req.userId) {
    return res.status(401).send({
      message: {
        type: 'error',
        body: 'Unauthorized'
      }
    })
  }

  try {
    // Get the User
    const user = await User.findById(req.params.id).exec()

    const email = user.email
    const name = user.username

    /**
     * If user doesn't have a band saved.
     */
    if (typeof user.band !== 'object') {
      // Delete User
      user.delete()

      // INFO LOG
      console.log(
        '\n>====================',
        '   USER DELETED.',
        '\n---------------------',
        '\n email : ', email,
        '\n name  : ', name,
        '\n====================>'
      )

      // Return Statement
      return res.send({
        message: {
          type: 'success',
          body: 'Account Deleted.'
        }
      })
    }

    // Get the Band
    const band = await Band.findById(user.band)
      .populate('bandPhotos')
      .populate('bandSongs')
      .populate('pageStyle')
      .exec()

    const bucketName = band.bandNameSlug

    // Delete Files & Delete Collections
    if (band.bandSongs !== null && band.bandSongs.length > 0) {
      band.bandSongs.forEach(async e => {
        await deleteFile(e.key)
        await Song.findByIdAndDelete(e)
      })
    }
    if (band.bandPhotos.length > 0) {
      band.bandPhotos.forEach(async e => {
        await deleteFile(e.key)
        await Photo.findByIdAndDelete(e)
      })
    }
    if (band.pageStyle != null &&
      typeof band.pageStyle.bgImageKey !== 'undefined' &&
      band.pageStyle.bgImageKey !== null) {
      await deleteFile(band.pageStyle.bgImageKey)
    }
    if (band.pageStyle !== null) {
      await Style.findByIdAndDelete(band.pageStyle)
    }
    if (band.techFileKey !== '') {
      await deleteFile(band.techFileKey)
      await Band.findByIdAndDelete(user.band)
    }

    // Delete s3 bandName folder
    if (user.hasFileSaved) {
      await deleteFile(bucketName)
    }

    // Delete Band and User
    await user.delete()
    await band.delete()

    // INFO LOG
    // console.log(
    //   '\n>====================',
    //   '\n  USER DELETED.      ',
    //   '\n---------------------',
    //   '\n email : ', email,
    //   '\n name  : ', name,
    //   '\n====================>'
    // )

    // Return Statement
    return res.status(200).send({
      message: {
        type: 'success',
        body: 'Account Deleted.'
      }
    })
  } catch (e) {
    console.log('DELETED ACCOUNT: ', e.message)
    // Return Statement
    return res.status(500).send({
      message: {
        type: 'error',
        body: 'Error when Deleted account..'
      }
    })
  }
}

/**
 * Change The password of the user.
 * @returns
 */
const changePassword = async (req, res) => {
  try {
    const user = await User.findById(req.userId).exec()

    const passwordIsValid = bcrypt.compareSync(
      req.body.oldPpassword,
      user.password
    )
    if (!passwordIsValid) {
      return res.send({ message: { type: 'error', body: 'Password does not match' } })
    }

    user.password = bcrypt.hashSync(req.body.newPassword, 10)
    await updateUser(user.id, { password: user.password })

    res.status(200).send({
      message: {
        type: 'success',
        body: 'Password is updated.'
      }
    })
    return
  } catch (e) {
    res.status(422).send({
      message: {
        type: 'error',
        body: mongooseErrorFormatter(e)
      }
    })
  }
}

/**
 * Get personnal data into a json file.
 * Write the json on the server AWS bucket, and make it accessible for a time Delay.
 * Then delete it from the S3 bucket.
 * @returns (status, message, url?, timeout?, minute?)
 */
const getDataToFile = async (req, res, next) => {
  if (req.params.id !== req.userId) {
    return res.status(401).send('Unauthorized.')
  }

  try {
    /**
     * Get the User
     */
    const user = await User.findById(req.params.id).exec()

    /**
     * Clean Object
     */
    user.password = undefined
    user.__v = undefined
    user.roles = undefined

    /**
     * Get and Populate the Band
     */
    const userBand = await Band.findById(user.band)
      .populate('pageStyle')
      .populate('bandSongs')
      .populate('bandPhotos')
      .exec()

    user.band = userBand

    /**
     * get Download File Url
     */
    await downloadPersonalData(user).then(data => {
      if (!data) {
        return res.status(500).send({
          message: {
            type: 'error',
            body: 'an error occurs'
          }
        })
      }

      /**
       * Return statement
       */
      return res.send({
        message: {
          type: 'success',
          body: 'File Url is Ready'
        },
        url: data.url,
        timeout: data.timeout,
        minute: data.minute
      })
    })

    // return;
  } catch (e) {
    console.log(e)
    return res.status(500).send({
      message: {
        type: 'error',
        body: 'error getting the file.'
      }
    })
  }
}

/**
 * Subscribe or Unsubcribe, to the mailing list.
 * req
 */
const subscribeMailinglist = (req, res) => {
  if (req.body.id !== req.userId) {
    return res.status(401).send({ message: 'unauthorized' })
  }

  User.findById(req.body.id)
    .exec(async (err, user) => {
      if (err) {
        console.log('subscribeMailinglist: ', err)
        return res.status(500).send({
          message: {
            type: 'error',
            body: 'an unknow error occurs'
          }
        })
      }

      if (!user) {
        return res.status(404).send({
          message: {
            type: 'error',
            body: 'User Not found.'
          }
        })
      }

      if (typeof req.body.choice !== 'boolean') {
        return res.status(403).send({
          message: {
            type: 'error',
            body: ''
          }
        })
      }

      try {
        let message = 'User choice is the same as already saved.'

        if (user.isOnMailingList === req.body.choice) {
          return res.status(200).send({ status: 'error', message })
        }

        await updateUser(user.id, { isOnMailingList: req.body.choice })

        message = !req.body.choice ? 'User unsubscribe to mailing List.' : 'User subscribe to mailing List.'

        // INFO LOG
        // console.log(
        //   '>----------------------------------',
        //   '\n user.controller                  :',
        //   '\n-----------------------------------',
        //   '\n\n==> SubscribeMailingList <==\n',
        //   '\n', message,
        //   '\n\n---------------------------------->'
        // )

        return res.status(200).send({
          message: {
            type: 'success',
            body: message
          },
          isOnMailingList: req.body.choice
        })
      } catch (e) {
        return res.status(500).send({
          message: {
            type: 'error',
            body: e.message
          }
        })
      }
    })
}

const subscribeSite = (req, res) => {
  User.findById(req.userId)
    .exec(async (err, user) => {
      if (err) {
        console.log('subscribeSite: ', err)
        return res.status(500).send({
          message: {
            type: 'error',
            body: ''
          }
        })
      }

      if (!user) {
        return res.status(404).send({
          message: {
            type: 'error',
            message: 'User Not found.'
          }
        })
      }

      try {
        const userSubscriptionChoice = !user.hasSubscribe
        await updateUser(user.id, { hasSubscribe: userSubscriptionChoice })

        const message = userSubscriptionChoice ? 'User has not subscribe.' : 'User has subscribe !'

        return res.status(200).send({
          message: {
            type: 'success',
            body: message
          },
          hasSubscribe: userSubscriptionChoice
        })
      } catch (e) {
        console.log(e.message)
        return res.status(500).send({
          message: {
            type: 'error',
            body: e.message
          }
        })
      }
    })
}

const getBandSite = async (req, res) => {
  let user
  try {
    user = await User.findById(req.userId).exec()
    if (!user) { return res.status(401).send({ status: 'error', message: 'no user found.' }) }
    // return user
  } catch (e) {
    console.log('Error getBandSite : ', e.message)
  }
  console.log('next')
  /**
   * Auth gmail for the apllication :
   * https://myaccount.google.com/lesssecureapps
   */
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'bandpromobox@gmail.com',
      pass: 'b6h7Zr6izq9JrcP'
    }
  })
  console.log('continue ...')

  const mailOptions = {
    from: 'BandPromoBox.com',
    to: 'bandpromobox@gmail.com',
    subject: 'BPB: Demande de création de site perso !',
    html: `<div style="background:#303030;color:#fff;text-align:center;padding: 1em;">
    <h2>Create My WebSite !</h2>
    <a style="display:block;width:max-content;margin:auto;background:dodgerblue;color:#fff;text-decoration:none;padding:.5em 1em;"
        target="_blank" href="https://bandpromobox.com">BandPromoBox.com</a>
    <table style="margin:1em auto;border:1px solid grey;">
        <tr>
            <td style="padding: .5em 1em; font-size: 1.25em;">id</td>
            <td style="padding: .5em 1em; font-size: 1.25em;">${user.id}</td>
        </tr>
        <tr>
            <td style="padding: .5em 1em; font-size: 1.25em;">Username</td>
            <td style="padding: .5em 1em; font-size: 1.25em;">${user.username}</td>
        </tr>
        <tr>
            <td style="padding: .5em 1em; font-size: 1.25em;">email</td>
            <td style="padding: .5em 1em; font-size: 1.25em;">${user.email}</td>
        </tr>
    </table>
    </div>`
  }

  // Send email And create the bandSite Files ("createMyBandSite()").
  transporter.sendMail(mailOptions, async (error, info) => {
    console.log('send mail..')
    if (error) {
      console.log(error)
      return res.status(500).send({ status: 'error', message: 'Message not sent: ' + error.message })
    } else {
      try {
        await createMyBandSite(user)
        // const url = await createMyBandSite(user)
        // if (url.error) {
        // console.log('Return Url after createMyBandSite error: ' + url.error)
        // } else {
        // user.bandSiteUrl = url.siteurl
        // await user.save()
        // }

        console.log('Email sent: ' + info.response)
        return res.status(200).send({ status: 'ok', message: 'Email sent: ' + info.response })
      } catch (e) {
        console.log('Error before mail well send: ', e.message)
      }
    }
  })
}

module.exports = {
  deleteAccount,
  changePassword,
  getDataToFile,
  subscribeMailinglist,
  subscribeSite,
  getBandSite
}
