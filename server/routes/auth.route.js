const express = require('express')
const router = express.Router()
const { verifyToken } = require('../middlewares/auth.middleware')
const authModule = require('../modules/auth.module')

/**
 * Set headers
 */
router.use((req, res, next) => {
  res.header(
    'Access-Control-Allow-Headers',
    'x-access-token, Origin, Content-Type, Accept'
  )
  next()
})

/**
 * Register a user
 */
router.post('/register', authModule.register)

/**
 * Login a user
 */
router.post('/login', authModule.login)

/**
 * Logout a user
 */
router.get('/logout', verifyToken, authModule.logout)

module.exports = router
