const express = require('express')
const router = express.Router()
const { verifyToken, getUserAndBand, verifyStorageSize } = require('../middlewares/auth.middleware')
const { hasFileRequest } = require('../middlewares/band.middleware')
const { upload } = require('../middlewares/upload.middleware')
const bandModule = require('../modules/bands/band.module')
const styleModule = require('../modules/styles/style.module')
const photoModule = require('../modules/photos/photo.module')
const songModule = require('../modules/songs/song.module')

/**
 * Set headers
 */
router.use((req, res, next) => {
  res.header(
    'Access-Control-Allow-Headers',
    'x-access-token, Origin, Content-Type, Accept'
  )
  next()
})

/**
 * Save a band
 */
router.post('/', [verifyToken, getUserAndBand, hasFileRequest], bandModule.saveBand)

/**
 * Get the technical file url link
 */
router.get('/get-techfile', verifyToken, bandModule.getTechFileUrl)

/**
 * Save the page style of the band
 */
router.post('/style', [
  verifyToken,
  getUserAndBand
  // verifyStorageSize('bandPhotos'),
  // upload
], styleModule.savePageStyle)

/**
 * Save the photos of the band
 */
router.post('/photo', [
  verifyToken,
  getUserAndBand,
  verifyStorageSize('bandPhotos'),
  upload
], photoModule.savePhoto)

/**
 * Get the photos link url of the band
 */
router.get('/photos', [verifyToken, getUserAndBand], photoModule.getUserPhotosUrl)

/**
 * Delete a photo
 */
router.delete('/photo/:id', [verifyToken, getUserAndBand], photoModule.deletePhoto)

/**
 * Save the songs of the band
 */
router.post('/song', [
  verifyToken,
  getUserAndBand,
  verifyStorageSize('bandSongs'),
  upload
], songModule.saveSong)

/**
 * Delete a song
 */
router.delete('/song/:id', [verifyToken, getUserAndBand], songModule.deleteSong)

module.exports = router
