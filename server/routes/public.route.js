const express = require('express')
const router = express.Router()
const { verifyUser } = require('../middlewares/auth.middleware')
const publicModule = require('../modules/public.module')

router.use(function (req, res, next) {
  res.header(
    'Access-Control-Allow-Headers',
    'x-access-token, Origin, Content-Type, Accept'
  )
  next()
})

/**
 * Get the last song saved (one / band) limited to 20 result
 */
router.get('/public-song', publicModule.getPublicSongs)

/**
 * Get the message sended from client and send email to bandpromobox@gmail.com
 */
router.post('/contact-us', verifyUser, publicModule.contactUs)

module.exports = router
