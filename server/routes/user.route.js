const express = require('express')
const router = express.Router()
const { verifyToken } = require('../middlewares/auth.middleware')
const userModule = require('../modules/users/user.module')

/**
 * Set headers
 */
router.use((req, res, next) => {
  res.header(
    'Access-Control-Allow-Headers',
    'x-access-token, Origin, Content-Type, Accept'
  )
  next()
})

/**
 * Delete Account
 */
router.delete('/delete-account/:id', verifyToken, userModule.deleteAccount)

/**
 * Change Password
 */
router.post('/change-password', verifyToken, userModule.changePassword)

/**
 * GetDatas
 */
router.post('/get-own-data/:id', verifyToken, userModule.getDataToFile)

/**
 * Mailing List
 */
router.post('/mailing-list', verifyToken, userModule.subscribeMailinglist)

/**
 * Subscribe
 */
router.post('/subscribe', verifyToken, userModule.subscribeSite)

/**
 * Get Band Site
 */
router.get('/get-band-site', verifyToken, userModule.getBandSite)

module.exports = router
