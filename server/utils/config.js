module.exports = {
  port: parseInt(process.env.PORT || 3000),
  mongoUrl: process.env.MONGO_URL || 'mongodb://localhost:27017/bandpromobox_node_server'
  // jwtSecret: process.env.JWT_SECRET,
  // awsId: process.env.AWS_ACCESS_ID,
  // awsSecret: process.env.AWS_ACCESS_SECRET,
  // awsBucket: process.env.AWS_BUCKET_NAME,
  // awsRegion: process.env.AWS_BUCKET_REGION
}
