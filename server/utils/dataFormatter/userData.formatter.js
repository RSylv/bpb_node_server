const jwt = require('jsonwebtoken')
const TIME_SESSION = 3600 // 1 hour
const Band = require('../db.models').band
const { getSignedFileUrl } = require('../fileUpload/uploadFile')

/**
 * Format User data for the session information, before sending it to the client.
 */
const userSessionDataFormatter = async (userData) => {
  const userSession = {}
  const token = jwt.sign({ id: userData._id }, process.env.JWT_SECRET, {
    expiresIn: TIME_SESSION
  })

  // ES6 Spread and destruction assignment syntax approach
  const { isOnline, password, isBanned, __v, ...user } = userData._doc
  userSession.user = user
  userSession.accessToken = token
  userSession.user.band = await getBandDatas(userData)

  return userSession
}

/**
 * Get the information of the band,
 * and add signedUrl to the documents saved in the AWS S3 bucket.
 * Return a Band object with all the data associated
 */
const getBandDatas = async (user) => {
  if (typeof user.band === 'undefined') { return null }
  const userBand = await Band.findById(user.band)
    .populate('pageStyle')
    .populate('bandSongs')
    .populate('bandPhotos')
    .exec()

  if (userBand) {
    if (userBand.pageStyle) {
      userBand.pageStyle.bgImageKey = await getSignedFileUrl(userBand.pageStyle.bgImageKey, TIME_SESSION)
    }
    userBand.techFileKey = await getSignedFileUrl(userBand.techFileKey, TIME_SESSION)
    for (let i = 0; i < userBand.bandPhotos.length; i++) {
      userBand.bandPhotos[i].path = await getSignedFileUrl(userBand.bandPhotos[i].key, TIME_SESSION)
    }
    for (let i = 0; i < userBand.bandSongs.length; i++) {
      userBand.bandSongs[i].path = await getSignedFileUrl(userBand.bandSongs[i].key, TIME_SESSION)
    }
  }

  return userBand
}

module.exports = {
  userSessionDataFormatter
}
