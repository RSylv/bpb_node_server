/**
 * Format mongoose errors.
 * Create new error object and push messages string into it.
 */
const mongooseErrorFormatter = (rawErrors) => {
  const errors = {}
  const details = rawErrors.errors
  for (const key in details) {
    errors[key] = [details[key].message]
  }
  return errors
}

module.exports = {
  mongooseErrorFormatter
}
