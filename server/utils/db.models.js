const mongoose = require('mongoose')
const db = {}

db.mongoose = mongoose
db.user = require('../modules/users/model/User')
db.band = require('../modules/bands/model/Band')

module.exports = db
