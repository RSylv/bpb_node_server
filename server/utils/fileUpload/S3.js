const AWS = require('aws-sdk')
// const AWS3 = require('aws-sdk/clients/s3');

const region = process.env.AWS_BUCKET_REGION
const accessKeyId = process.env.AWS_ACCESS_ID
const secretAccessKey = process.env.AWS_ACCESS_SECRET

const s3Client = new AWS.S3({
  region,
  accessKeyId,
  secretAccessKey
})

const S3 = {

  async get (fileName, bucket) {
    const params = {
      Bucket: bucket,
      Key: fileName
    }

    let data = await s3Client.getObject(params).promise()

    if (!data) {
      throw Error(`Failed to get file ${fileName}, from ${bucket}`)
    }

    if (/\.json$/.test(fileName)) {
      data = JSON.parse(data.Body.toString())
    }

    return data
  },

  async write (data, fileName, bucket, ACL, ContentType) {
    const params = {
      Bucket: bucket,
      Body: Buffer.isBuffer(data) ? data : JSON.stringify(data),
      Key: fileName,
      ACL,
      ContentType
    }
    // console.log('params', params)

    const newData = await s3Client.putObject(params).promise()

    if (!newData) {
      throw Error('there was an error writing the file')
    }

    return newData
  },

  async getSignedURL (bucket, fileName, expriySeconds) {
    return s3Client.getSignedUrl('getObject', {
      Bucket: bucket,
      Key: fileName,
      Expires: expriySeconds
    })
  },

  async deleteFile (bucket, fileName) {
    const deleteParams = {
      Key: fileName,
      Bucket: bucket
    }

    return s3Client.deleteObject(deleteParams).promise()
  }
}

exports.S3 = S3
