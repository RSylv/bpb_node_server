/**
 * ----------------------------
 * |  UPLOAD FILE MIDDLEWARE  |
 * ----------------------------
 * - 20 05 2021
 *
 * Save, Delete, manage file of the AWS S3 Bucket.
 * input: data file (base64), output: return the promise from aws.
 *
 * Files type :
 * - photo    - image
 * - song     - audio
 * - techFile - image or pdf
 * - userData - json
 *
 * We ask aws to create a signedUrl to authorize file access from the client.
 *
 * For user data we create a json file based on the user data object,
 * and we save temporally the file it to the bucket.
 *
 */
// require('dotenv').config()
const FileType = require('file-type')
const S3 = require('./S3').S3
const randStr = require('../tools').getRandomString

const BUCKET_NAME = process.env.AWS_BUCKET_NAME
const imageTypes = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif']
const audioTypes = ['audio/mpeg', 'audio/flac']
const PERSONNAL_FILE_OPTIONS = { ext: '.txt', fullExt: 'application/txt' }

/**
  * Make some verifications for the incoming file,
  * and upload it to the AWS S3 bucket.
  *
  * (Only **AUDIO** (mpeg/flac) OR **IMAGE** (jpeg/jpg/png/gif))
  * @returns
  */
const uploadFile = async (body = { file, type, path }) => {
  try {
    /**
     * If body is empty, or if missing informations
     * return.
     */
    if (!body || !body.file || !body.type) {
      return { status: 'error', message: 'incorrect request' }
    }

    /**
     * Get the right mimetypes array, depend of the file type.
     * and check if type is allowed.
     */
    const allowedMimes = body.type.slice(0, 5) === 'audio'
      ? audioTypes
      : body.type.slice(0, 5) === 'image'
        ? imageTypes
        : ['application/pdf']

    if (!allowedMimes.includes(body.type)) {
      return { status: 'error', message: `mime is not allowed. Only ${allowedMimes.map(e => e.substr(6, e.length))}` }
    }

    /**
     * Get the fileData and pass it into a buffer.
     */
    const fileData = body.file.slice(body.file.indexOf('base64') + 7, body.file.length)
    // console.log(fileData.substr(0, 25));
    const buffer = Buffer.from(fileData, 'base64')

    /**
     * Then check the validity of the type with the FileType npm package.
     * (Verification is based on the 'magical number')
     * If types of FileType response and body.type doesn't match return an error.
     */
    const fileInfo = await FileType.fromBuffer(buffer)
    console.log(fileInfo)
    const detectedExt = fileInfo.ext
    const detectedMime = fileInfo.mime
    if (detectedMime !== body.type) {
      return { status: 'error', message: 'mime types dont match' }
    }

    /**
     * Create a random file name, ad the file Key.
     * Then upload it to AWS S3 Bucket.
     */
    const name = randStr(32)
    const key = `${body.path}/${name}.${detectedExt}`

    // console.log(`writing file to bucket called ${key}`)

    await S3.write(buffer, key, BUCKET_NAME, null, body.type)

    return {
      status: 'ok',
      // fileUrl: url,
      key: key
    }
  } catch (e) {
    console.log('uploadFile error: ', e)
    /**
     * If error occurs during the process, return it.
     */
    return { status: 'error', message: 'failed to upload file.' }
  }
}

/**
 * Delete file from the AWS S3 bucket.
 * @param {string} key key of the file to delete.
 * @returns Promise<PromiseResult<AWS.S3.DeleteObjectOutput, AWS.AWSError>>
 */
const deleteFile = (key) => {
  return S3.deleteFile(BUCKET_NAME, key)
}

/**
 * Get an access url for a file, in AWS S3 Bucket.
 * @param {string} key Key of the file.
 * @param {number} expirySeconds Time the link is valid.
 * @returns Promise<string | { status: 'error'; message: any; }>
 */
const getSignedFileUrl = async (key = '', expirySeconds = 1200) => {
  if (key === '') {
    return { status: 'error', message: 'Key can\'t be empty' }
  }
  try {
    return await S3.getSignedURL(BUCKET_NAME, key, expirySeconds)
  } catch (e) {
    return { status: 'error', message: e.message || 'failed to get the file url' }
  }
}

/**
 * Get the user data, and save it into file, (he is stringify into the S3 write function)
 * downloadable by the user.
 * Set a timeout to delete the file after it was generated.
 * @param {object} userData
 * @returns
 */
const downloadPersonalData = async (userData) => {
  /**
    * Name of the file
    */
  const fileName = new Date().toLocaleDateString() + new Date().getTime() + Math.floor(Math.random() * 10000000) + PERSONNAL_FILE_OPTIONS.ext
  fileName.replace('-', '_')

  try {
    const bucket = BUCKET_NAME + '/temp'

    /**
      * Write data into the temp bucket
      */
    await S3.write(userData, fileName, bucket, null, PERSONNAL_FILE_OPTIONS.fullExt)

    /**
      * Get signed url to access it
      */
    const url = await S3.getSignedURL(bucket, fileName, 120) // 2 min (s)

    /**
      * Delete the file after the timeout
      */
    setTimeout(() => S3.deleteFile(bucket, fileName), 60000) // 1min (ms)

    return {
      status: 'ok',
      url,
      timeout: 60000,
      minute: 1
    }
  } catch (error) {
    console.log(error)
    return false
  }
}

const getFile = async (fileName) => {
  try {
    return await S3.get(fileName, BUCKET_NAME)
  } catch (e) {
    return { status: 'error', message: e.message || 'failed to save file locally' }
  }
}

module.exports = {
  uploadFile,
  deleteFile,
  getSignedFileUrl,
  downloadPersonalData,
  getFile
}
