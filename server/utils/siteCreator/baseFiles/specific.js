
const table = document.getElementById('members');
const slider = document.getElementById('slider');

// SET USER DATAS

// members
let members = [
    { "pseudo": "Jeannot", "role": "Guitare" },
    { "pseudo": "Danielle", "role": "Contre-Basse" },
    { "pseudo": "Denis", "role": "Banjo" }
]
let events = [
    { "nom": "Jeannot", "role": "Guitare" },
    { "nom": "Danielle", "role": "Contre-Basse" },
    { "nom": "Denis", "role": "Banjo" }
]
let description = [
    {desc: "ila faoihsdlfds sdf sd"},
    {desc: "Mpk oihlks sds"},
    {desc: "Tf sdfsdf sdfsdi omiklpoeroô ze"},
    {desc: "PAZazep apze aze!!"},
]

members.forEach(m => {
    const tr = document.createElement("tr")
    tr.innerHTML = '<td>'+m.pseudo+'</td><td>→</td><td>'+m.role+'</td>'
    table.appendChild(tr)
})

// images
for (let i = 0; i <= 3; i++) {
    const image = document.createElement("img")
    if (i === 0) { image.className = "slider-active" }
    image.src = './assets/photo/0'+(i + 1)+'.jpg';
    image.dataset.desc = description[i].desc
    slider.appendChild(image)
}

// audio
const audio = document.getElementById('audio')
const audioTracks = ['LostInCave.mp3', 'weCry.mp3', 'daubaus.mp3', 'Alanaise.mp3', 'floralMai.mp3', 'Neigal.mp3', 'Sonik.mp3']
let audioCount = 0;
let nbrAudio = audioTracks.length;

audio.src = './assets/audio/' + audioTracks[audioCount]

// Audio Play button & bars
const spanBars = document.querySelectorAll('.play-animation>span')
const buttonPlay = document.querySelectorAll('.play-btn')
const animationPlayer = document.querySelectorAll('.play-animation')
const nextBtn = document.querySelectorAll('.nextSong')
const prevBtn = document.querySelectorAll('.prevSong')
const audioTitle = document.querySelectorAll('.audio-title>span')

buttonPlay.forEach((btn, idx) => {
    btn.addEventListener('click', () => {
        buttonPlay.forEach(b => {
            b.childNodes[1].classList.toggle('hidden')
            b.childNodes[3].classList.toggle('hidden')

            if (b.childNodes[1].classList.contains('hidden')) {
                audio.play()
                showTitle(audioTracks[audioCount])
            } else {
                audio.pause()
                showTitle()
            }
        })
        spanBars.forEach(s => {
            s.classList.toggle('anim-bars')
        })
        animationPlayer.forEach(a => {
            a.classList.toggle('hidden')
        })
    })
})

nextBtn.forEach(btn => btn.addEventListener('click', nextSong));
prevBtn.forEach(btn => btn.addEventListener('click', prevSong));
function nextSong() {
    audioCount++
    if (audioCount > nbrAudio - 1) {
        audioCount = 0;
    }
    audio.pause()
    audio.currentTime = 0;
    audio.src = './assets/audio/' + audioTracks[audioCount]
    showTitle(audioTracks[audioCount])
    audio.play();
}
function prevSong() {
    audioCount--
    if (audioCount < 0) {
        audioCount = nbrAudio - 1;
    }
    audio.pause()
    audio.currentTime = 0;
    audio.src = './assets/audio/' + audioTracks[audioCount]
    showTitle(audioTracks[audioCount])
    audio.play();
}
function showTitle(title = '') {
    audioTitle.forEach(at => {
        if (title !== '') {
            at.innerHTML = "<strong>"+title.slice(0, title.lastIndexOf('.'))+"</strong>-<em>L\'osträk Alempsue</em>";
        } else {
            at.innerHTML = '';
        }
    })
}
