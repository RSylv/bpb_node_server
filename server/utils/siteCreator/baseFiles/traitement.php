<?php

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $sendEmail = true;

    // CHECK THE EMAIL
    if (isset($_POST['email']) && !empty($_POST['email'])) {
        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $sendEmail = false;
            return returnJsonHttpResponse(422, "Veuillez rentrer un email valide.", 0);
        } else {
            $email = sanitize($_POST['email']);
            unset($_POST['email']);
        }
    } else {
        $sendEmail = false;
        return returnJsonHttpResponse(422, "Veuillez rentrer un email.", 0);
    }

    // CHECK THE MESSAGE
    if (isset($_POST['message']) && !empty($_POST['message']) && strlen(trim($_POST['message'])) > 0) {
        if (2500 < strlen($_POST['message'])) {
            $sendEmail = false;
            return returnJsonHttpResponse(422, "Votre message est trop long.. 1500 caracteres maximum.", 1);
        } else {
            $messageForm = sanitize($_POST['message']);
            unset($_POST['message']);
        }
    } else {
        $sendEmail = false;
        return returnJsonHttpResponse(422, "Veuillez rentrer un message.", 1);
    }

    if ($sendEmail === true) {


        /////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////        ENVOI DU MESSAGE        ///////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////


        $to = $_SESSION["email"];
        $sujet = (isset($objectForm) ? $objectForm : "Nouveau message sur votre site BandPromoBox");
        printf("%s", $sujet);

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= "From: BandPromoBox@no-reply.com" . PHP_EOL . 'X-Mailer: PHP/' . phpversion();
        $messageForm = wordwrap($messageForm, 75, PHP_EOL);

        $message = '
            <div style="padding:1.5rem; text-align: center; background: dodgerblue; color: white; border-radius: 0.2rem;">
                <h1 style="font-size:1.7rem;">
                    <a style="color:white;cursor:pointer;text-decoration:none;" href="https://bandpromobox.com" target="_blank">
                        BandPromoBox
                    </a>
                </h1>
                <h2 style="color:white;">Vous avez recu un message !</h2>
                <p style="color:white;">Quelqun vous a contacté sur votre site BandPromoBox ! une date en vue peut-etre !! ;) </p>
                <div style="text-align: left;">
                    <p style="color:white;">
                        Message de : <span style="color: black;"> ' . $nameSurname . ' </span> <br>
                        email : <span style="color: black;"><a href="' . $email . '" target="_blank"> ' . $email . ' </a></span> <br>
                        Telephone : <span style="color: black;"> ' . (isset($phone) ? $phone : "<em>non renseigné</em>") . ' </span> <br>
                        Objet : <span style="color: black;"> ' . (isset($objectForm) ? $objectForm : "<em>non renseigné</em>") . ' </span>
                    </p>
                    <h4>Message :<h4>
                    <div style="text-align: justify; background: white; border-radius: 0.3rem; padding:1rem; word-wrap: break-word;">
                        <span style="color: black;"> ' . $messageForm . ' </span>
                    </div>
                </div>
            </div>
            ';

        $result = mail($to, $sujet, $message, $headers);

        if ($result) {
            return returnJsonHttpResponse(200, "Votre message a bien été envoyé, merci !");
        } else {
            return returnJsonHttpResponse(500, "Erreur dans l'envoi du message. Veuillez compléter a nouveau le formulaire..");
        }
        exit();
    } else {
        header("Location: index.php#contact");
        exit();
    }
    
} else {
    return returnJsonHttpResponse(405, "Method not allowed.");
}


/**
 * Check if it exist, and sanitise the data.
 *
 * @param string $data
 * @return boolean - data or false
 */
function sanitize(string $data = '')
{
    $data = htmlspecialchars($data);
    $data = htmlentities($data);
    $data = strip_tags($data);
    $data = stripslashes($data);
    $data = trim($data);

    return $data;
}


/**
 * Retourne un header et une reponse en json
 *
 * @param int $code
 * @param string $msg
 * @param int $id
 */
function returnJsonHttpResponse($code, $msg, $id = null)
{
    // remove any string that could create an invalid JSON 
    // such as PHP Notice, Warning, logs...
    ob_clean();

    // clear the old headers
    header_remove();

    // set the actual code
    http_response_code($code);

    // treat this as json
    header("Content-Type: application/json");

    // encode your PHP Object or Array into a JSON string.
    // stdClass or array
    $data = json_encode(array(
        'id' => $id,
        'message' => $msg
    ));

    // print
    print_r($data);

    // making sure nothing is added
    exit();
}
