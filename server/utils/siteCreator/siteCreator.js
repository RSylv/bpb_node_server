const fs = require('fs')
const { getFile } = require('../fileUpload/uploadFile')
const Band = require('../db.models').band
const SAVE_DIR = './public/temp'
const BASE_DIR = './server/utils/siteCreator/baseFiles'
const TIMEOUT = 30000

/**
 * Get All the data of the band and create/write the files with the user datas.
 *
 * @param {User} user
 */
async function createMyBandSite (user) {
  try {
    // Get the band user dats
    const band = await Band.findById(user.band)
      .populate('bandPhotos')
      .populate('bandSongs')
      .populate('pageStyle')
      .exec()

    console.log(
      '\n\n***********************',
      '\n* BAND SITE CREATOR : *',
      '\n***********************',
      '\n\n--> User: \n', user,
      '\n\n\n--> Band: \n', band,
      '\n***********************'
    )

    const directory = `${SAVE_DIR}/${band.bandNameSlug}`
    await createDir(directory)
    getAndSetFiles(directory, band)
    setTimeout(async () => { await generateAndCopyFiles(directory, band) }, TIMEOUT)
    return { status: true, siteurl: `${band.bandNameSlug}.com` }
  } catch (error) {
    console.log('createMyBandSite error: ', error)
    return { status: false, error }
  }
}

/**
 * CREATE THE FOLDER
 * - assets
 * - assets/photo
 * - assets/audio
 *
 * DOWNLOAD AND SAVE THE FILES
 * - assets/background(.jpg)
 * - assets/fileTech_bandNameSlug.pdf
 * - assets/photo/photoName(.jpg) ... foreach
 * - assets/audio/songName(.jpg) ... foreach
 *
 * @param {string | fs.PathLike} path A path to a file. If a URL is provided, it must use the file: protocol.
 * To tests the path, URL support is experimental.
 * @param {Band} band The band informations (band table from database), of the user.
 */
const getAndSetFiles = async (path, band) => {
  try {
    await createDir(`${path}/assets`)
    await createDir(`${path}/assets/photo`)
    await createDir(`${path}/assets/audio`)
    await createDir(`${path}/assets/img`)

    // Audios
    await band.bandSongs.forEach(async (audio, idx) => {
      try {
        const ext = getExtension(audio.key)
        const a = await getFile(audio.key)
        if (a.status !== 'ok') { console.log('AWS audio: ', a) }
        await writeFileToDisk(`${path}/assets/audio/0${idx + 1}.${ext}`, a.Body)
        audio.path = `./assets/audio/0${idx + 1}.${ext}`
      } catch (error) {
        console.log('bandSongs Error: ', error)
      }
    })

    // Background
    const back = await getFile(band.pageStyle.bgImageKey)
    if (back.status !== 'ok') { console.log(back) }
    await writeFileToDisk(`${path}/assets/background.${getExtension(band.pageStyle.bgImageKey)}`, back.Body)
    band.pageStyle.bgImageKey = `./assets/background.${getExtension(band.pageStyle.bgImageKey)}`

    // FileTech
    const fileT = await getFile(band.techFileKey)
    const fileExt = getExtension(band.techFileKey)
    if (fileT.status !== 'ok') { console.log(fileT) }
    await writeFileToDisk(`${path}/assets/techFile_${band.bandNameSlug}.${fileExt}`, fileT.Body)
    band.techFileKey = `./assets/techFile_${band.bandNameSlug}.${fileExt}`

    // Photos
    await band.bandPhotos.forEach(async (photo, idx) => {
      try {
        const ext = getExtension(photo.key)
        const p = await getFile(photo.key)
        if (p.status !== 'ok') { console.log('AWS photo: ', p) }
        await writeFileToDisk(`${path}/assets/photo/0${idx + 1}.${ext}`, p.Body)
        photo.name = `0${idx + 1}.${ext}`
        photo.path = `./assets/photo/0${idx + 1}.${ext}`
      } catch (error) {
        console.log('bandSongs Error: ', error)
      }
    })

    // Img
    copyFile('scotch.png', `${path}/assets/img`)
  } catch (error) {
    console.log('AWS GET AND SAVE-TO-DISK ERROR: ', error)
    // throw error;
  }
}

/**
 * CREATE THE FOLDER
 * - js
 * - css
 *
 * CREATE THE FILES
 * - index.html
 * - traitement.php
 * - css/style.css
 * - js/specific.js
 *
 * AND Copy the other base files.
 *
 * @param {string | fs.PathLike} path A path to a file. If a URL is provided, it must use the file: protocol.
 * To tests the path, URL support is experimental.
 * @param {Band} band The band informations (band table from database), of the user.
 * @returns
 */
const generateAndCopyFiles = async (path, band) => {
  try {
    await createDir(`${path}/js`)
    await createDir(`${path}/css`)
  } catch (error) {
    // throw error;
    console.log('generateAndCopyFiles, created folder: ', error)
  }

  /**
   * Get the user datas.
   * Reconstruct the array of datas, with only the needed keys/values.
   */
  const membersB = []
  const eventsB = []
  const audiosB = []
  const photosB = []

  band.members.forEach(mem => { membersB.push({ pseudo: mem.pseudo, role: mem.role }) })
  band.events.forEach(ev => {
    eventsB.push({ price: ev.price, date: ev.date, city: ev.city, zipCode: ev.zipCode, place: ev.place, hour: ev.hour })
  })
  band.bandSongs.forEach(bs => {
    audiosB.push({ type: bs.type, path: bs.path, size: bs.size, name: bs.name })
  })
  band.bandPhotos.forEach(bp => {
    photosB.push({ type: bp.type, path: bp.path, size: bp.size, name: bp.name, description: bp.description })
  })

  /**
   * Get the font name to call google font import
   * Ex. 'Bangers Slim', cursive = Bangers+Slim
   */
  const fontName = band.pageStyle.font.slice(1, band.pageStyle.font.lastIndexOf("'")).replace(/ /ig, '+')

  /**
   * HTML
   * create the index.html file.
   */
  const htmlFile = `
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>${band.bandName}</title>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=${fontName}&display=swap" rel="stylesheet"> 
    
    <style>
    :root {
        --main-bg-color: ${band.pageStyle.bgColor};
        --main-text-color: ${band.pageStyle.textColor};
        --main-title-color: ${band.pageStyle.titleColor};
        --main-font-title: ${band.pageStyle.font};
        --main-shadow-color: ${band.pageStyle.shadowColor};
        --main-band-name-color: ${band.pageStyle.bandNameColor};;
    }
    </style>

    <link rel="stylesheet" href="./css/style.min.css">

</head>

<body>

    <header>
        <nav>
            <div class="menu-player hidden">
                <div class="player">
                    <div class="audio-controls">
                        <button class="btn prevSong">◄</button>
                        <button class="btn play-btn">
                            <span class="play-triangle"></span>
                            <span class="pause-bars hidden"></span>
                        </button>
                        <button class="btn nextSong">►</button>
                    </div>
                    <div class="track-info">
                        <div class="play-animation hidden">
                            <span></span><span></span><span></span><span></span><span></span>
                        </div>
                        <div class="audio-title">
                            <span></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="menu-name"></div>
            <div class="menu-links">
                <a class="btn" href="#info">Info</a>
                <a class="btn" href="#gallery">Galerie</a>
                <a class="btn" href="#contact">Contact</a>
            </div>
            <div class="btn burger-menu">
                MENU
            </div>
        </nav>
        <div class="menu-responsive hidden">
            <a class="btn" href="#info">Info</a>
            <a class="btn" href="#gallery">Galerie</a>
            <a class="btn" href="#contact">Contact</a>
        </div>
    </header>

    <!-- audio -->
    <audio id="audio" controls src="" style="display:none">
        Your browser does not support the
        <code>audio</code> element.
    </audio>

    <div class="first-page" id="top">
        <h1>${band.bandName}</h1>
        <div class="player">
            <div class="audio-controls">
                <button class="btn prevSong">◄</button>
                <button class="btn play-btn">
                    <span class="play-triangle"></span>
                    <span class="pause-bars hidden"></span>
                </button>
                <button class="btn nextSong">►</button>
            </div>
            <div class="play-animation hidden">
                <span></span><span></span><span></span><span></span><span></span>
            </div>
            <div class="audio-title">
                <span></span>
            </div>
        </div>
    </div>

    <section class="band-info" id="info">

        <div class="members">
            <h2>Membres</h2>
            <table id="members"></table>
            <h3>Fiche Technique</h3>
            <div class="tech-file-link">
                <a href="${band.techFileKey}" target="_blank">
                    <div class="pdf">PDF</div>
                </a>
            </div>
            <p><em>Cliquer ci-dessus pour télécharger le document.</em></p>
        </div>

        <div class="biographie">
            <h2>Biographie</h2>
            <div id="biographie">
                ${band.biographie}
            </div>
        </div>

    </section>

    <section id="gallery">
        <h2>Galerie</h2>
        <div class="photo-count"><span id="currentPhoto"></span> / <span id="totalPhotos"></span></div>
        <div class="photo-desc" id="photoDesc"></div>
        <div class="gallery">
            <div class="slider" id="slider"></div>
            <button class="btn left">◄</button>
            <button class="btn right">►</button>
        </div>
    </section>

    <section class="event">
        <h2>Evenements</h2>
        <div id="events"></div>
    </section>

    <section class="contact" id="contact">
        <h2>Contactez-nous</h2>
        <form>

            <div class="form-input email">
                <label for="email">
                    Email
                </label>
                <input type="email" name="email" id="email" placeholder="email@host.com">
            </div>
            <div class="error-message"></div>

            <div class="form-input">
                <label for="message">
                </label>
                <textarea name="message" id="message" rows="5" placeholder="Ecrivez ici votre message.."
                    maxlength="2500">
                </textarea>
            </div>
            <div class="error-message"></div>
            <div class="success" id="successMsg"></div>

            <div class="form-input">
                <input type="submit" value="Envoyer" class="btn submit">
            </div>

        </form>
    </section>

    <footer>
        ${band.bandName} - Copyright &copy; 2021
    </footer>

    <a class="top-btn hidden btn" href="#top">▲</a>

    <script src="./js/specific.js" type="text/javascript"></script>
    <script src="./js/main.min.js" type="text/javascript"></script>
    <script src="./js/slider.min.js" type="text/javascript"></script>
    <script src="./js/form.min.js" type="text/javascript"></script>

</body>

</html>
  `
  try {
    await writeFileToDisk(`${path}/index.html`, htmlFile)
  } catch (error) {
    console.log('writeFileToDisk _ htmlFile: ', error)
  }

  /**
   * PHP
   *
   * the traitement.php file with user email.
   */
  const phpFile = `<?php  
if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $sendEmail = true;

    // CHECK THE EMAIL
    if (isset($_POST['email']) && !empty($_POST['email'])) {
        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $sendEmail = false;
            return returnJsonHttpResponse(422, "Veuillez rentrer un email valide.", 0);
        } else {
            $email = sanitize($_POST['email']);
            unset($_POST['email']);
        }
    } else {
        $sendEmail = false;
        return returnJsonHttpResponse(422, "Veuillez rentrer un email.", 0);
    }

    // CHECK THE MESSAGE
    if (isset($_POST['message']) && !empty($_POST['message']) && strlen(trim($_POST['message'])) > 0) {
        if (2500 < strlen($_POST['message'])) {
            $sendEmail = false;
            return returnJsonHttpResponse(422, "Votre message est trop long.. 1500 caracteres maximum.", 1);
        } else {
            $messageForm = sanitize($_POST['message']);
            unset($_POST['message']);
        }
    } else {
        $sendEmail = false;
        return returnJsonHttpResponse(422, "Veuillez rentrer un message.", 1);
    }

    if ($sendEmail === true) {

        /////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////        ENVOI DU MESSAGE        ///////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////


        $to = "${band.emailContact}";
        $sujet = (isset($objectForm) ? $objectForm : "Nouveau message sur votre site BandPromoBox");
        printf("%s", $sujet);

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= "From: BandPromoBox@no-reply.com" . PHP_EOL . 'X-Mailer: PHP/' . phpversion();
        $messageForm = wordwrap($messageForm, 75, PHP_EOL);

        $message = '
            <div style="padding:1.5rem; text-align: center; background: dodgerblue; color: white; border-radius: 0.2rem;">
                <h1 style="font-size:1.7rem;">
                    <a style="color:white;cursor:pointer;text-decoration:none;" href="https://bandpromobox.com" target="_blank">
                        BandPromoBox
                    </a>
                </h1>
                <h2 style="color:white;">Vous avez recu un message !</h2>
                <p style="color:white;">Quelqun vous a contacté sur votre site BandPromoBox ! une date en vue peut-etre !! ;) </p>
                <div style="text-align: left;">
                    <p style="color:white;">
                        Message de : <span style="color: black;"> ' . $nameSurname . ' </span> <br>
                        email : <span style="color: black;"><a href="' . $email . '" target="_blank"> ' . $email . ' </a></span> <br>
                        Telephone : <span style="color: black;"> ' . (isset($phone) ? $phone : "<em>non renseigné</em>") . ' </span> <br>
                        Objet : <span style="color: black;"> ' . (isset($objectForm) ? $objectForm : "<em>non renseigné</em>") . ' </span>
                    </p>
                    <h4>Message :<h4>
                    <div style="text-align: justify; background: white; border-radius: 0.3rem; padding:1rem; word-wrap: break-word;">
                        <span style="color: black;"> ' . $messageForm . ' </span>
                    </div>
                </div>
            </div>
            ';

        $result = mail($to, $sujet, $message, $headers);

        if ($result) {
            return returnJsonHttpResponse(200, "Votre message a bien été envoyé, merci !");
        } else {
            return returnJsonHttpResponse(500, "Erreur dans l'envoi du message. Veuillez compléter a nouveau le formulaire..");
        }
        exit();
    } else {
        header("Location: index.php#contact");
        exit();
    }
    
} else {
    return returnJsonHttpResponse(405, "Method not allowed.");
}


/**
 * Check if it exist, and sanitise the data.
 *
 * @param string $data
 * @return boolean - data or false
 */
function sanitize(string $data = '')
{
    $data = htmlspecialchars($data);
    $data = htmlentities($data);
    $data = strip_tags($data);
    $data = stripslashes($data);
    $data = trim($data);

    return $data;
}


/**
 * Retourne un header et une reponse en json
 *
 * @param int $code
 * @param string $msg
 * @param int $id
 */
function returnJsonHttpResponse($code, $msg, $id = null)
{
    // remove any string that could create an invalid JSON 
    // such as PHP Notice, Warning, logs...
    ob_clean();

    // clear the old headers
    header_remove();

    // set the actual code
    http_response_code($code);

    // treat this as json
    header("Content-Type: application/json");

    // encode your PHP Object or Array into a JSON string.
    // stdClass or array
    $data = json_encode(array(
        'id' => $id,
        'message' => $msg
    ));

    // print
    print_r($data);

    // making sure nothing is added
    exit();
}
exit();
     ?>
  `

  try {
    await writeFileToDisk(`${path}/traitement.php`, phpFile)
  } catch (error) {
    console.log('writeFileToDisk _ phpFile: ', error)
  }

  /**
   * CSS
   *
   * copy the file 'style.min.css' from baseFiles to temp folder.
   */
  copyFile('style.min.css', `${path}/css`)

  /**
   * JAVASCRIPT
   *
   * create the javascript files.
   */
  copyFile('form.min.js', `${path}/js`)
  copyFile('main.min.js', `${path}/js`)
  copyFile('slider.min.js', `${path}/js`)

  /**
   * Images/Icons
   */
  copyFile('scotch.png', `${path}/assets/img`)

  /**
   * Specific Js File
   * A js file with userBand Array data (members, events, audio, photo) and specific data.
   */
  const specificJs = `
const table = document.getElementById('members');
const slider = document.getElementById('slider');
const events = document.getElementById('events');

// SET USER DATAS

// members
let membersB = ${JSON.stringify(membersB)}
let eventsB = ${JSON.stringify(eventsB)}
let description = ${JSON.stringify(photosB)}

membersB.forEach(m => {
    const tr = document.createElement("tr")
    tr.innerHTML = '<td>'+m.pseudo+'</td><td>→</td><td>'+m.role+'</td>'
    table.appendChild(tr)
})

const ul = document.createElement("ul")
eventsB.forEach(e => {
    const li = document.createElement("li")

    const divHour = document.createElement("div")
    divHour.innerHTML = '<span>'+ new Date(e.date).toDateString('%A, %B %e, %Y') + '</span><span>'+ e.hour +'</span>';
    divHour.className = 'ev-hour-date';

    const spanCity = document.createElement("span")
    spanCity.innerHTML = '<strong>' + e.city + '</strong>, <em>' + e.zipCode + '</em>';
    spanCity.className = 'ev-place';

    const spanPrice = document.createElement("span")
    spanPrice.textContent = e.price + '€';
    spanPrice.className = 'ev-price';

    const spanPlace = document.createElement("span")
    spanPlace.innerHTML = 'Lieu: <strong>' + e.place + '</strong>';

    li.append(divHour, spanCity, spanPrice, spanPlace)
    ul.append(li)
})
events.append(ul)

// images
for (let i = 0; i < ${photosB.length}; i++) {
    const image = document.createElement("img")
    if (i === 0) { image.className = "slider-active" }
    image.src = './assets/photo/0' + (i + 1) + '.jpg';
    image.dataset.desc = description[i].description
    slider.appendChild(image)
}

// audio
const audio = document.getElementById('audio')
const audioTracks = ${JSON.stringify(audiosB)}
let audioCount = 0;
let nbrAudio = audioTracks.length;

audio.src = audioTracks[audioCount].path

// Audio Play button & bars
const spanBars = document.querySelectorAll('.play-animation>span')
const buttonPlay = document.querySelectorAll('.play-btn')
const animationPlayer = document.querySelectorAll('.play-animation')
const nextBtn = document.querySelectorAll('.nextSong')
const prevBtn = document.querySelectorAll('.prevSong')
const audioTitle = document.querySelectorAll('.audio-title>span')

buttonPlay.forEach((btn, idx) => {
    btn.addEventListener('click', () => {
        buttonPlay.forEach(b => {
            b.childNodes[1].classList.toggle('hidden')
            b.childNodes[3].classList.toggle('hidden')

            if (b.childNodes[1].classList.contains('hidden')) {
                audio.play()
                showTitle(audioTracks[audioCount].name)
            } else {
                audio.pause()
                showTitle()
            }
        })
        spanBars.forEach(s => {
            s.classList.toggle('anim-bars')
        })
        animationPlayer.forEach(a => {
            a.classList.toggle('hidden')
        })
    })
})

nextBtn.forEach(btn => btn.addEventListener('click', nextSong));
prevBtn.forEach(btn => btn.addEventListener('click', prevSong));
function nextSong() {
    audioCount++
    if (audioCount > nbrAudio - 1) {
        audioCount = 0;
    }
    audio.pause()
    audio.currentTime = 0;
    audio.src = audioTracks[audioCount].path
    showTitle(audioTracks[audioCount].name)
    audio.play();
}
function prevSong() {
    audioCount--
    if (audioCount < 0) {
        audioCount = nbrAudio - 1;
    }
    audio.pause()
    audio.currentTime = 0;
    audio.src = audioTracks[audioCount].path
    showTitle(audioTracks[audioCount].name)
    audio.play();
}
function showTitle(title = '') {
    audioTitle.forEach(at => {
        if (title !== '') {
            at.innerHTML = "<strong>" + title + "</strong>-<em>${band.bandName}</em>";
        } else {
            at.innerHTML = '';
        }
    })
}
    `
  try {
    await writeFileToDisk(`${path}/js/specific.js`, specificJs)
  } catch (error) {
    console.log('writeFileToDisk _ specificJs: ', error)
  }

  return true
}

/**
 * Delete the files into the bandNameSlug folder, if an error occurs
 * @param {string} bandNameSlug
 */
const deleteChunkFiles = (bandNameSlug) => {
  console.log('#TODO: if something wrong delete the folders created. (bandNameSlug)')
}

/**
 * Check if a file in assets folder already exists.
 * @param {file} file
 * @returns {boolean}
 */
const isFileAlreadyExists = (file) => {
  console.log('#TODO: verify if file in path exist, and if it is the same file.')
}

/*******************/
/* TOOLS FUNCTIONS */
/*******************/

/**
 * Synchronously tests whether or not the given path exists by checking with the file system.
 * Asynchronous mkdir(2) - create a directory with a mode of 0o777.
 *
 * @param {fs.PathLike} path — A path to a file. If a URL is provided, it must use the file: protocol.
 * To tests the path, URL support is experimental.
 */
function createDir (path) {
  if (!fs.existsSync(path)) {
    fs.mkdir(path, (err) => {
      if (err) {
        throw err
      }
      console.log('Folders created...')
      return true
    })
  }
}

/**
 * Asynchronously writes data to a file, replacing the file if it already exists.
 *
 * @param {number | fs.PathLike} path
 * A path to a file. If a URL is provided, it must use the file: protocol.
 * URL support is experimental. If a file descriptor is provided, the underlying file will not be closed automatically.
 * @param {string | NodeJS.ArrayBufferView} data
 * The data to write.
 * If something other than a Buffer or Uint8Array is provided, the value is coerced to a string.
 * @param {string} encoding
 * If encoding is not supplied, the default of 'utf8' is used.
 * @return {boolean | throw err}
 */
function writeFileToDisk (path, data, encoding = 'utf8') {
  fs.writeFile(path, data, encoding, (err) => {
    if (err) {
      throw err
    }
    console.log('File has been written !')
    return true
  })
}

function getExtension (elem) {
  return elem.slice(elem.lastIndexOf('.') + 1, elem.length)
}

function copyFile (filename, path) {
  fs.copyFile(`${BASE_DIR}/${filename}`, `${path}/${filename}`, (err) => {
    if (err) { console.log(`Error Copying ${filename} File: `, err) }
    return true
  })
}

module.exports = {
  createMyBandSite
}
