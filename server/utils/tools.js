/**
 * Generate a random string.
 * Default Length = 23.
 *
 * @param {number} length Default = 23
 * @returns random string
 */
const getRandomString = (length = 23) => {
  const randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  let result = ''
  for (let i = 0; i < length; i++) {
    result += randomChars.charAt(Math.floor(Math.random() * randomChars.length))
  }
  return result
}

/**
 * Equivalent of PHP htmlspecialchars
 * credit: Charles Stover.
 * @param {string} string the string to escape
 * @returns escaped string
 */
const htmlspecialchars = (string) => {
  let escapedString = string

  // For each of the special characters,
  const len = htmlspecialchars.specialchars.length
  for (let x = 0; x < len; x++) {
    // Replace all instances of the special character with its entity.
    escapedString = escapedString.replace(
      new RegExp(htmlspecialchars.specialchars[x][0], 'g'),
      htmlspecialchars.specialchars[x][1]
    )
  }

  return escapedString
}

// A collection of special characters and their entities.
htmlspecialchars.specialchars = [
  ['&', '&amp;'],
  ['<', '&lt;'],
  ['>', '&gt;'],
  ['"', '&quot;']
]

module.exports = {
  getRandomString,
  htmlspecialchars
}
