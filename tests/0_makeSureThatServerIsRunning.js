const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('../server')

chai.use(chaiHttp)
chai.should()

describe('Make sure that the server is up and running', () => {
  it('Should return status 200', (done) => {
    chai.request(app)
      .get('/')
      .end((err, res) => {
        if (err) { return done(err) }
        res.should.have.status(200)
        done()
      })
  })
})

describe('Make sure that the 404 response is working', () => {
  it('Should return status 404', (done) => {
    const adress = new Date().getTime()
    chai.request(app)
      .get(`/${adress}`)
      .end((err, res) => {
        if (err) { return done(err) }
        res.should.have.status(404)
        done()
      })
  })
  it('Should return a message text', (done) => {
    const adress = new Date().getTime()
    chai.request(app)
      .get(`/${adress}`)
      .end((err, res) => {
        if (err) { return done(err) }
        res.text.should.contain('Rien par ici')
        done()
      })
  })
})
