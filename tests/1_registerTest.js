const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('../server')
const userTest = require('./__dataTest').userTest

chai.use(chaiHttp)
chai.should()

describe('Make sure register fails on no data', () => {
  it('Should return status code 422', (done) => {
    const agent = chai.request.agent(app)
    agent
      .post('/auth/register')
      .end((err, res) => {
        if (err) { return done(err) }
        res.should.have.status(422)
        done()
      })
  })
  it('Should return error message "Error during registration"', (done) => {
    const agent = chai.request.agent(app)
    agent
      .post('/auth/register')
      .end((err, res) => {
        if (err) { return done(err) }
        res.body.message.should.have.property('body').eql('Error during registration')
        done()
      })
  })
  it('Response should contain error message "Password is required"', (done) => {
    const agent = chai.request.agent(app)
    agent
      .post('/auth/register')
      .end((err, res) => {
        if (err) { return done(err) }
        res.body.should.have.property('errors')
        res.text.should.contain('Password is required')
        done()
      })
  })
  it('Response should contain error message "Email is required"', (done) => {
    const agent = chai.request.agent(app)
    agent
      .post('/auth/register')
      .end((err, res) => {
        if (err) { return done(err) }
        res.body.should.have.property('errors')
        res.text.should.contain('Email is required')
        done()
      })
  })
  it('Response should contain error message "Name is required"', (done) => {
    const agent = chai.request.agent(app)
    agent
      .post('/auth/register')
      .end((err, res) => {
        if (err) { return done(err) }
        res.body.should.have.property('errors')
        res.text.should.contain('Name is required')
        done()
      })
  })
})

describe('Make sure register fails on invalid Email', () => {
  it('Should return status code 422', (done) => {
    const agent = chai.request.agent(app)
    agent
      .post('/auth/register')
      .send({
        username: 'John',
        email: 'john@sss',
        password: '12345678'
      })
      .end((err, res) => {
        if (err) { return done(err) }
        res.should.have.status(422)
        done()
      })
  })
  it('Should return error message "Error during registration"', (done) => {
    const agent = chai.request.agent(app)
    agent
      .post('/auth/register')
      .send({
        username: 'John',
        email: 'john@sss',
        password: '12345678'
      })
      .end((err, res) => {
        if (err) { return done(err) }
        res.body.should.have.property('message')
        res.body.message.should.have.property('body').eql('Error during registration')
        done()
      })
  })
  it('Response should contain error message "Email is not valid"', (done) => {
    const agent = chai.request.agent(app)
    agent
      .post('/auth/register')
      .send({
        username: 'John',
        email: 'john@sss',
        password: '12345678'
      })
      .end((err, res) => {
        if (err) { return done(err) }
        res.text.should.contain('Email is not valid')
        done()
      })
  })
})

describe('Make sure register is successful with valid data', () => {
  it('Should return status code 200, and "success" message:type', (done) => {
    const agent = chai.request.agent(app)
    agent
      .post('/auth/register')
      .type('form')
      .send({
        username: userTest.name,
        email: userTest.email,
        password: userTest.password
      })
      .end((err, res) => {
        if (err) { return done(err) }
        res.should.have.status(200)
        res.body.should.have.property('message')
        res.body.message.should.have.property('type').eql('success')
        res.body.message.should.have.property('body').eql('User was registered successfully')
        done()
      })
  })
})

describe('Make sure register fail if email already exists', () => {
  it('Should return response with status 422', (done) => {
    const agent = chai.request.agent(app)
    agent
      .post('/auth/register')
      .type('form')
      .send({
        username: userTest.name,
        email: userTest.email,
        password: userTest.password
      })
      .end((err, res) => {
        if (err) { return done(err) }
        res.should.have.status(422)
        done()
      })
  })
  it('Should return a body message "Error during registration"', (done) => {
    const agent = chai.request.agent(app)
    agent
      .post('/auth/register')
      .type('form')
      .send({
        username: userTest.name,
        email: userTest.email,
        password: userTest.password
      })
      .end((err, res) => {
        if (err) { return done(err) }
        res.body.message.should.have.property('body').eql('Error during registration')
        done()
      })
  })
  it('Should have property errors in the response body', (done) => {
    const agent = chai.request.agent(app)
    agent
      .post('/auth/register')
      .type('form')
      .send({
        username: userTest.name,
        email: userTest.email,
        password: userTest.password
      })
      .end((err, res) => {
        if (err) { return done(err) }
        res.body.should.have.property('errors')
        done()
      })
  })
  it('Should return a message "Email already exists"', (done) => {
    const agent = chai.request.agent(app)
    agent
      .post('/auth/register')
      .type('form')
      .send({
        username: userTest.name,
        email: userTest.email,
        password: userTest.password
      })
      .end((err, res) => {
        if (err) { return done(err) }
        res.text.should.contain('Email already exists')
        done()
      })
  })
})
