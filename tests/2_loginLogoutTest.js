const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('../server')
const User = require('../server/utils/db.models').user
const userTest = require('./__dataTest').userTest
let token = ''

chai.use(chaiHttp)
chai.should()

describe('Make sure login fails with no data', () => {
  it('Should return error with Missing credentials in the response', (done) => {
    const agent = chai.request.agent(app)
    agent
      .post('/auth/login')
      .end((err, res) => {
        if (err) { return done(err) }
        res.should.have.status(400)
        done()
      })
  })
  it('Response should return error in message:body with "Missing credentials"', (done) => {
    const agent = chai.request.agent(app)
    agent
      .post('/auth/login')
      .end((err, res) => {
        if (err) { return done(err) }
        res.body.should.have.property('message')
        res.body.message.should.have.property('body').equal('Missing credentials')
        done()
      })
  })
})

describe('Make sure login fails with an email that doesn\'t exist in the database', () => {
  const email = `john.${new Date().getTime()}@example.com`
  const agent = chai.request.agent(app)
  it('Should return status 404', (done) => {
    agent
      .post('/auth/login')
      .send({
        email,
        password: '12345678'
      })
      .end((err, res) => {
        if (err) { return done(err) }
        res.should.have.status(404)
        done()
      })
  })
  it('Response should contain "Email does not exist" in message:body', (done) => {
    agent
      .post('/auth/login')
      .send({
        email,
        password: '12345678'
      })
      .end((err, res) => {
        if (err) { return done(err) }
        res.should.have.status(404)
        res.body.should.have.property('message')
        res.body.message.should.have.property('body').eql('Email does not exist')
        done()
      })
  })
})

describe('Make sure login fails with an invalid password', () => {
  User.findOne({}, { email: 1 })
    .then(user => {
      it('Should return status 401', (done) => {
        const agent = chai.request.agent(app)
        agent
          .post('/auth/login')
          .type('form')
          .send({
            email: user.email,
            password: new Date().getTime()
          })
          .end((err, res) => {
            if (err) { return done(err) }
            res.should.have.status(401)
            done()
          })
      })
      it('Response should contain "Password is incorrect" in message:body', (done) => {
        const agent = chai.request.agent(app)
        agent
          .post('/auth/login')
          .type('form')
          .send({
            email: user.email,
            password: new Date().getTime()
          })
          .end((err, res) => {
            if (err) { return done(err) }
            res.body.should.have.property('message')
            res.body.message.should.have.property('body').eql('Password is incorrect')
            done()
          })
      })
    })
})

describe('Make sure login is working with correct datas, and well returning the JWToken', () => {
  it('should return success response, User is logged in message and the jwt token', (done) => {
    const agent = chai.request.agent(app)
    agent
      .post('/auth/login')
      .type('form')
      .send({
        email: userTest.email,
        password: userTest.password
      })
      .end((err, res) => {
        if (err) { return done(err) }
        res.should.have.status(200)
        res.text.should.contains('success')
        res.text.should.contains('User is logged in')
        res.body.data.should.have.property('accessToken')
        res.body.data.user.should.have.property('_id')
        res.body.data.user.should.have.property('username')
        res.body.data.user.should.have.property('email')
        res.body.data.user.should.have.property('createdAt')
        res.body.data.user.should.have.property('updatedAt')
        res.body.data.user.should.have.property('hasSubscribe').eql(false)
        res.body.data.user.should.have.property('isOnMailingList').eql(false)
        res.body.data.user.should.have.property('hasSiteActive').eql(false)
        res.body.data.user.should.have.property('bandSiteUrl').eql('none')
        token = res.body.data.accessToken
        done()
      })
  })
})

describe('Make sure that logout is working with the token set', () => {
  it('should return a success response with User has been logged out message', (done) => {
    const agent = chai.request.agent(app)
    agent
      .get('/auth/logout')
      .set('x-access-token', token)
      .end((err, res) => {
        if (err) { return done(err) }
        res.should.have.status(200)
        res.text.should.contain('User has been logged out')
        done()
      })
  })
})
