const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('../server')
const bandTest = require('./__dataTest').bandTest
const userTest = require('./__dataTest').userTest
const dataTest = {}

chai.use(chaiHttp)
chai.should()

/**
 * In this test case, we have to create a user, connect him to get the token, and then
 * test the route of the band creation.
 * First We try to send no data to show if the right error is send by the server
 * Then we test the band save route with minimum data
 * and a second time with full data.
 * We could like this test the create and upate function
 * At the end we try to delete the account.
 */

describe('Connect the temp user to get the JWToken, and test the band save functionnality', () => {
  it('Should return success response, User is logged in message and the jwt token', (done) => {
    const agent = chai.request.agent(app)
    agent
      .post('/auth/login')
      .type('form')
      .send({
        email: userTest.email,
        password: userTest.password
      })
      .end((err, res) => {
        if (err) { return done(err) }
        res.should.have.status(200)
        res.text.should.contains('success')
        res.text.should.contains('User is logged in')
        res.body.data.should.have.property('accessToken')
        dataTest.token = res.body.data.accessToken
        dataTest.id = res.body.data.user._id
        done()
      })
  })
})

describe('Make sure Band Save fails with no data', () => {
  const agent = chai.request.agent(app)
  it('Should return status 422', (done) => {
    agent
      .post('/auth/band')
      .set('x-access-token', dataTest.token)
      .end((err, res) => {
        if (err) { return done(err) }
        res.should.have.status(422)
        done()
      })
  })
  it('Response body should have property message:type with "error"', (done) => {
    agent
      .post('/auth/band')
      .set('x-access-token', dataTest.token)
      .end((err, res) => {
        if (err) { return done(err) }
        res.body.should.have.property('message')
        res.body.message.should.have.property('type').eql('error')
        done()
      })
  })
  it('Response body should contain "Band name is required"', (done) => {
    agent
      .post('/auth/band')
      .set('x-access-token', dataTest.token)
      .end((err, res) => {
        if (err) { return done(err) }
        res.should.have.status(422)
        res.text.should.contain('error')
        res.text.should.contain('Band name is required')
        done()
      })
  })
})

describe('Make sure Band Save works with needed data', () => {
  it('should return success message and status 200 in the response', (done) => {
    const agent = chai.request.agent(app)
    agent
      .post('/auth/band')
      .set('x-access-token', dataTest.token)
      .send({
        bandName: 'Test Band',
        emailContact: 'test@test.com'
      })
      .end((err, res) => {
        if (err) { return done(err) }
        res.should.have.status(201)
        res.text.should.contain('success')
        res.text.should.contain('Band created successfully')
        done()
      })
  })
})

describe('Make sure Band Save works with correct full data', () => {
  it('should return success message and status 200 in the response', (done) => {
    const agent = chai.request.agent(app)
    agent
      .post('/auth/band')
      .set('x-access-token', dataTest.token)
      .send({
        bandName: bandTest.bandName,
        videoLink: bandTest.videoLink,
        emailContact: bandTest.emailContact,
        phoneContact: bandTest.phoneContact,
        biographie: bandTest.biographie,
        members: bandTest.members,
        events: bandTest.events
      })
      .end((err, res) => {
        if (err) { return done(err) }
        res.should.have.status(200)
        res.text.should.contain('success')
        res.text.should.contain('Band was updated successfully')
        done()
      })
  })
})

describe('Make sure deleted account works', () => {
  it('should return success message and status 200 in the response', (done) => {
    const agent = chai.request.agent(app)
    agent
      .delete('/auth/user/delete-account/' + dataTest.id)
      .set('x-access-token', dataTest.token)
      .end((err, res) => {
        if (err) { return done(err) }
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.message.should.have.property('type').eql('success')
        res.body.message.should.have.property('body').eql('Account Deleted.')
        done()
      })
  })
})
