const userTest = {
  name: 'John Doe',
  email: 'john.doe@example.com',
  password: 'john.doe@example.com'
}

const bandTest = {
  bandName: 'Test Band',
  videoLink: 'https://www.youtube.com/watch?v=W38Bs0GpgKQ',
  emailContact: 'test@test.com',
  phoneContact: '0123456789',
  biographie: 'Fuga ab ea eum voluptatem, nihil consequuntur voluptatum assumenda dignissimos libero nam eaque hic id molestias aliquam? Provident a nostrum iure ea harum consectetur id adipisci, nulla fuga maxime impedit. Nisi quibusdam harum asperiores laudantium, beatae commodi natus quos molestiae. Hic, nam, veniam aperiam voluptates error est, a ipsa illum fugit esse officiis dolorum! Expedita deleniti sunt earum animi reiciendis! Animi blanditiis alias deleniti natus cupiditate quos accusamus veniam vel, doloribus, quae autem expedita culpa eveniet qui quidem. Ducimus voluptatum alias magnam consequuntur accusantium dolorum explicabo, quisquam omnis tenetur officia. Qui veritatis, fugiat omnis accusantium dicta debitis odit velit, voluptatum soluta iusto deserunt. Nihil dolorum quidem nemo consectetur, tempore, molestias corporis modi labore explicabo fugit nobis itaque iste deserunt doloribus.',
  members: [{ pseudo: 'Jeannot', role: 'Guitare, Chant' }, { pseudo: 'Maryze', role: 'Batterie' }],
  events: [{ date: new Date().toLocaleDateString(), place: 'bar test', city: 'Berlin', zipCode: '07160', price: 0, hour: '20:30' }]
}

module.exports = {
  userTest,
  bandTest
}
